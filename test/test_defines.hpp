#pragma once

#define QUASAR_TEST_DATA_NO 100
#define QUASAR_TEST_DATA_VALUE 206.5850067138671875f
#define QUASAR_TEST_NAME "SDSS J073733.01+392037.4"
#define QUASAR_TEST_TYPE "QSO"
#define QUASAR_TEST_FIT_TYPE "SERENDIPITY_BLUE"
#define QUASAR_TEST_MJD 51884

/**
 * Jeżeli istnieje definicja, to zostaną uruchomione testy wymagające połączenia z bazą danych MongoDB.
 */
#define MONGODB_TEST_LIVE
#define MONGODB_TEST_LIVE_NOTE "\n\tNOTE: MongoDB must be running for this test to work properly!"
#define MONGODB_TEST_HOST "localhost"
#define MONGODB_TEST_PORT 27017
#define MONGODB_TEST_DB "quasar_test"
/**
 * Jeżeli zdefiniowane, to zostanę uruchomione testy wydajnościowe współpracy z bazą danych.
 */
//#define MONGODB_TEST_PERFORMANCE
