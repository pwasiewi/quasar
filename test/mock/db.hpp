#pragma once

#include "quasar/db/db.hpp"

#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>
#include <ctime>

MOCK_BASE_CLASS(MockParameterizationResultSet, db::ParameterizationResultSet)
{
	public:
		MockParameterizationResultSet() :
					db::ParameterizationResultSet("name", std::time(nullptr), ParameterizationOptions())
		{

		}

		MOCK_METHOD(saveResults, 2, void(std::vector<Quasar>, ParaResults))

	};

MOCK_BASE_CLASS(MockQuasarCursor, db::QuasarCursor)
{
	public:
		MockQuasarCursor() :
					db::QuasarCursor()
		{

		}

		MOCK_METHOD(more, 0, bool())
		MOCK_METHOD(next, 1, std::vector<Quasar>(const size_t))
	};

MOCK_BASE_CLASS(MockQuasarSet, db::QuasarSet)
{
	public:
		MockQuasarSet(const std::string& name, const std::time_t date, const std::time_t insertDate,
				const size_t size) :
					db::QuasarSet(name, date, insertDate, size)
		{

		}

		MOCK_METHOD(getQuasarCursor, 0, std::shared_ptr<db::QuasarCursor>())
		MOCK_METHOD(getQuasars, 1, std::vector<Quasar>(size_t))
		MOCK_METHOD(addQuasars, 1, void(std::vector<Quasar>))
	};

MOCK_BASE_CLASS(MockConnection, db::Connection)
{
	public:
		MockConnection(const db::connectionParams& params) :
					db::Connection(params)
		{

		}

		virtual std::shared_ptr<db::QuasarSet> getNewQuasarSet(const std::string& name, const std::time_t date,
				const std::time_t insertDate, const size_t size) const
				{
			auto qs = new MockQuasarSet(name, date, insertDate, size);
			MOCK_EXPECT(qs->addQuasars);
			return std::shared_ptr<db::QuasarSet>(qs);
		}

		MOCK_METHOD(connect, 0, void())
		MOCK_METHOD(getNewParaResultSet, 4, std::shared_ptr<db::ParameterizationResultSet>(const std::string&, const std::time_t, const typename db::ParameterizationResultSet::ParameterizationOptions, const std::string&))
		MOCK_METHOD(saveParaResultSet, 1, void(std::shared_ptr<db::ParameterizationResultSet>))
		MOCK_METHOD(getQuasarSetByName, 1, std::shared_ptr<db::QuasarSet>(const std::string&))
		MOCK_METHOD(getQuasarSets, 0, std::unique_ptr<db::Connection::QuasarSets>())
		MOCK_METHOD(saveQuasarSet, 1, void(std::shared_ptr<db::QuasarSet>))
		MOCK_METHOD(deleteQuasarSet, 1, void(std::shared_ptr<db::QuasarSet>))
		MOCK_METHOD(isConnected, 0)
		MOCK_METHOD(getDBType, 0)
	};

inline void setup_mock(MockConnection* c)
{
	MOCK_EXPECT(c->isConnected).returns(true);
	MOCK_EXPECT(c->getDBType).returns(db::DBType::MONGODB);
}
