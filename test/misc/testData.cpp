#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_DATA
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"

#include "quasar/data/windows.hpp"

/**
 *
 */
BOOST_AUTO_TEST_SUITE(DataTest)

/**
 *
 */
BOOST_AUTO_TEST_CASE(FeWinFullTest)
{
	BOOST_CHECK(!data::fewinfull.empty());
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ContWinFullTest)
{
	BOOST_CHECK(!data::contwinfull.empty());
}

BOOST_AUTO_TEST_SUITE_END()
