#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_CCFITS
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"

#include "quasar/common/common.hpp"

#include <CCfits/CCfits>
#include <vector>
#include <memory>
#include <string>
#include <limits>
#include <cmath>
#include <iostream>
#include <ios>
#include <valarray>

/**
 *
 */
BOOST_AUTO_TEST_SUITE(CCfitsTest)

const std::string fitPath("test/test_data/test.fit");

/**
 *
 */
BOOST_AUTO_TEST_CASE(OpenTest)
{
	BOOST_CHECK_NO_THROW(std::shared_ptr<CCfits::FITS> file(new CCfits::FITS(fitPath, CCfits::Read)));
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ReadKeysTest)
{
	std::cout << "FITS read test:" << std::endl;

	std::shared_ptr<CCfits::FITS> file;
	BOOST_CHECK_NO_THROW(file.reset(new CCfits::FITS(fitPath, CCfits::Read)));

	file->pHDU().readAllKeys();

	int mjd, plate, fiber = 0;
	double a, b, z, ra, dec = 0;
	file->pHDU().readKey("MJD", mjd);
	file->pHDU().readKey("FIBERID", fiber);
	file->pHDU().readKey("PLATEID", plate);

	file->pHDU().readKey("COEFF0", a);
	file->pHDU().readKey("COEFF1", b);
	file->pHDU().readKey("Z", z);
	file->pHDU().readKey("RAOBJ", ra);
	file->pHDU().readKey("DECOBJ", dec);

	typedef std::numeric_limits<double> dbl;
	std::cout.precision(dbl::digits10);

	std::cout << "\t" << common::quasar::coord2name(ra, dec) << std::endl;
	std::cout << "\tmjd: " << mjd << std::endl;
	std::cout << "\tfiber: " << fiber << std::endl;
	std::cout << "\tplate: " << plate << std::endl;
	std::cout << "\ta: " << std::fixed << a << std::endl;
	std::cout << "\tb: " << std::fixed << b << std::endl;
	std::cout << "\tra: " << std::fixed << ra << std::endl;
	std::cout << "\tdec: " << std::fixed << dec << std::endl;
	std::cout << "\tz: " << std::fixed << z << std::endl;
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ReadDataTest)
{
	std::shared_ptr<CCfits::FITS> file;
	BOOST_CHECK_NO_THROW(file.reset(new CCfits::FITS(fitPath, CCfits::Read)));

	auto& phdu = file->pHDU();

	std::valarray<double> fluxo;
	std::valarray<double> ferr;

	long n(phdu.axis(0));
	BOOST_CHECK_NO_THROW(phdu.read(fluxo, {1,1}, n));
	BOOST_CHECK_NO_THROW(phdu.read(ferr, {1,3}, n));

	BOOST_CHECK_EQUAL(fluxo.size(), ferr.size());
}

BOOST_AUTO_TEST_SUITE_END()
