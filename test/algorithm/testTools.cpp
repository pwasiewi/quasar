#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_TOOLS
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/algorithm/tools/Tools.hpp"
#include "quasar/algorithm/utils.hpp"

#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_TOOLS
{
		SF_OCL_TOOLS()
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

		}
		~SF_OCL_TOOLS()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(ToolsTest, SF_OCL_TOOLS)

std::vector<cl_float> host_chisq(const std::vector<cl_float>& fs, const std::vector<cl_float>& ys,
		const std::vector<cl_float>& errors, const size_t rows, const size_t row_size)
{
	std::vector<cl_float> results(rows);
	for (size_t i = 0; i < rows; i++)
	{
		float chisq = 0;
		for (size_t j = 0; j < row_size; j++)
		{
			size_t idx = i * row_size + j;
			chisq += ((fs[idx] - ys[idx]) * (fs[idx] - ys[idx])) / (errors[idx] * errors[idx]);
		}
		results[i] = chisq;
	}
	return results;
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ConstructorTest)
{
	namespace acl = algorithm::ocl;

	BOOST_CHECK_NO_THROW(acl::Tools tools(context, queue));
}

BOOST_AUTO_TEST_CASE(ConvolveTest)
{
	double device_time = 0;

	long size = 4096;
	long filtrSize = 9;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> specs_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> filtr_vec(filtrSize, 1);
	std::vector<cl_uint> sizes_vec(size, QUASAR_DATA_SIZE);

	cl_float i = 1.0;
	std::generate(filtr_vec.begin(), filtr_vec.end(),
			[&]()
			{
				cl_float t = i;
				i += 1.0;
				return t;
			});

	// Bufor cl::Buffer na spec
	auto specs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto out = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto filtr = au::make_buffer<cl_float>(context, CL_MEM_READ_ONLY, filtrSize);

	cl::copy(queue, specs_vec.begin(), specs_vec.end(), specs);
	cl::copy(queue, filtr_vec.begin(), filtr_vec.end(), filtr);
	cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);

	std::shared_ptr<acl::Tools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::Tools(context, queue)));

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->convolve(specs, QUASAR_DATA_SIZE, size, sizes, filtr, filtrSize, out, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po splocie
		std::vector<cl_float> convolved(QUASAR_DATA_SIZE, 0);
		queue.finish();
		cl::copy(queue, out, convolved.begin(), convolved.end());
		queue.finish();

		// TODO: Porównanie wartości i czasów.

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);

	std::cout << "ConvolveTest:" << std::endl;
	std::cout << "\tconvolve(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
}

BOOST_AUTO_TEST_CASE(CopyIfInfTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	double device_time = 0;

	long size = 4096;
	long output_row_size = 256;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_float> output_vec(size * output_row_size, 111);

	int i = 0;
	std::generate(data_vec.begin(), data_vec.end(),
			[&]()
			{
				i++;
				if(i%4 != 0)
				return INFINITY;
				return static_cast<float>(i);
			});

	// Bufory
	auto data = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto output = au::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, size * output_row_size);

	std::shared_ptr<acl::Tools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::Tools(context, queue)));

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), data);
			cl::copy(queue, output_vec.begin(), output_vec.end(), output);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->copyIfNotInf(data, QUASAR_DATA_SIZE, size, output, output_row_size, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, output, output_vec.begin(), output_vec.end());
		queue.finish();

		// Sprawdzenie poprawności.
		for (auto i : output_vec)
		{
			BOOST_CHECK_NE(i, INFINITY);
			BOOST_CHECK_GT(i, 0.f);
		}

		// TODO: Porównanie czasów.

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "CopyIfNotInfTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tcopyIfNotInf(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms"
			<< std::endl;
}

BOOST_AUTO_TEST_CASE(CountIfNotInfTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t size = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_uint> new_sizes_vec(size, 0);
	std::vector<cl_uint> sizes_vec(size, QUASAR_DATA_SIZE);

	int i = 0;
	std::generate(data_vec.begin(), data_vec.end(),
			[&]()
			{
				i++;
				if(i%6 != 0)
				return INFINITY;
				return static_cast<float>(i);
			});

	// Bufory
	auto data = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto data_trans = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto new_sizes = au::make_buffer(context, CL_MEM_READ_ONLY, new_sizes_vec);

	std::shared_ptr<acl::Tools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::Tools(context, queue)));

	acl::BasicMatrix bm(context, queue);

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), data);
			bm.transpose(data, QUASAR_DATA_SIZE, size, data_trans);

			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, new_sizes_vec.begin(), new_sizes_vec.end(), new_sizes);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->countIfNotInf(data_trans, size, QUASAR_DATA_SIZE, new_sizes, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, new_sizes, new_sizes_vec.begin(), new_sizes_vec.end());
		queue.finish();

		// Wykonanie na host
		std::vector<cl_uint> host_results_vec(QUASAR_DATA_SIZE, 0);
		host_time += test::measure<>::execution([&]()
		{
			for(size_t i = 0; i < size; i++)
			{
				size_t row_count = 0;
				size_t row_size = sizes_vec[i];
				size_t idx = i * QUASAR_DATA_SIZE;
				for(size_t j = 0; j < row_size; j++)
				{

					if(data_vec[idx] != INFINITY && data_vec[idx] != -INFINITY)
					{
						row_count++;
					}
					idx++;
				}
				host_results_vec[i] = row_count;
			}
		});

		// Porównanie wyników host i device
		BOOST_CHECK_EQUAL_COLLECTIONS(host_results_vec.begin(), host_results_vec.end(),
				new_sizes_vec.begin(), new_sizes_vec.end());

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;
	host_time /= max_i;

	std::cout << "CountIfNotInfTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tcountIfNotInf(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_countIfNotInf(), liczba: " << size << ", czas: " << host_time << " ms" << std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(ReglinTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	double device_time = 0;

	long size = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> xs_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> ys_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_uint> sizes_vec(size, 4096);
	std::vector<cl_float8> results_vec(QUASAR_DATA_SIZE, { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f });

	int i = 0;
	int b = 0;
	std::generate(ys_vec.begin(), ys_vec.end(),
			[&]()
			{
				i++;
				if(i%(QUASAR_DATA_SIZE+1) == 0)
				{
					i = 1;
					b++;
				}
				return static_cast<float>(i+b);
			});

	i = 0;
	std::generate(xs_vec.begin(), xs_vec.end(),
			[&]()
			{
				i++;
				if(i%(QUASAR_DATA_SIZE+1) == 0)
				i=1;
				return static_cast<float>(i);
			});

	// Bufory
	auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto xs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto ys = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto results = au::make_buffer(context, CL_MEM_READ_ONLY, results_vec);

	std::shared_ptr<acl::Tools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::Tools(context, queue)));

	acl::BasicMatrix bm(context, queue);

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, xs_vec.begin(), xs_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, xs);

			cl::copy(queue, ys_vec.begin(), ys_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, ys);

			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, results_vec.begin(), results_vec.end(), results);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->reglin(xs, ys, size, QUASAR_DATA_SIZE, sizes, results, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, results, results_vec.begin(), results_vec.end());
		queue.finish();

		// TODO: Porównanie wartości i czasów.

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "ReglinTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\treglin(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
}

BOOST_AUTO_TEST_CASE(TrapzTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	double device_time = 0;

	long size = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> xs_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> ys_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_uint> sizes_vec(size, 4096);
	std::vector<cl_float> results_vec(QUASAR_DATA_SIZE, 0.0f);

	int i = 0;
	std::generate(ys_vec.begin(), ys_vec.end(),
			[&]()
			{
				i++;
				return static_cast<float>(i);
			});

	i = 0;
	std::generate(xs_vec.begin(), xs_vec.end(),
			[&]()
			{
				i++;
				return static_cast<float>(i);
			});

	// Bufory
	auto xs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto ys = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto results = au::make_buffer(context, CL_MEM_READ_ONLY, results_vec);

	std::shared_ptr<acl::Tools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::Tools(context, queue)));

	acl::BasicMatrix bm(context, queue);

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

			cl::copy(queue, xs_vec.begin(), xs_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, xs);

			cl::copy(queue, ys_vec.begin(), ys_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, ys);

			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, results_vec.begin(), results_vec.end(), results);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->trapz(xs, ys, size, QUASAR_DATA_SIZE, sizes, results, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, results, results_vec.begin(), results_vec.end());
		queue.finish();

		for(int i = 0; i < 100 ; i++)
		{
			BOOST_CHECK_CLOSE(results_vec[i], static_cast<cl_float>(8388608 * (2 * i + 1)), 0.1);
		}

		// TODO: Porównanie czasów.

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "TrapzTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\ttrapz(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
}

BOOST_AUTO_TEST_CASE(ChisqTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	long size = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> fs_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> ys_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> errors_vec(size * QUASAR_DATA_SIZE, 0.01);
	std::vector<cl_uint> sizes_vec(size, 4096);
	std::vector<cl_float> results_vec(QUASAR_DATA_SIZE, 0.0f);

	int i = 0;
	std::generate(ys_vec.begin(), ys_vec.end(),
			[&]()
			{
				i++;
				float randf = ((float)rand()/(float)(RAND_MAX)) * 0.3f;
				return static_cast<float>(i+randf);
			});

	i = 0;
	std::generate(fs_vec.begin(), fs_vec.end(),
			[&]()
			{
				i++;
				return static_cast<float>(i);
			});

	// Bufory
	auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto fs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto ys = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto errors = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto results = au::make_buffer(context, CL_MEM_READ_ONLY, results_vec);

	std::shared_ptr<acl::Tools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::Tools(context, queue)));

	acl::BasicMatrix bm(context, queue);

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, fs_vec.begin(), fs_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, fs);

			cl::copy(queue, errors_vec.begin(), errors_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, errors);

			cl::copy(queue, ys_vec.begin(), ys_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, ys);

			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, results_vec.begin(), results_vec.end(), results);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->chisq(fs, ys, errors, size, QUASAR_DATA_SIZE, sizes, results, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, results, results_vec.begin(), results_vec.end());
		queue.finish();

		std::vector<cl_float> host_results_vec;
		host_time += test::measure<>::execution([&]()
		{
			host_results_vec = host_chisq(fs_vec, ys_vec, errors_vec, QUASAR_DATA_SIZE, size);
		});

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_results_vec.begin(), host_results_vec.end(),
				results_vec.begin(), results_vec.end(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;
	host_time /= max_i;

	std::cout << "ChisqTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tchisq(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_chisq(), liczba: " << size << ", czas: " << host_time << " ms" << std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
