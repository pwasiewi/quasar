#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_FEFITWIN
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/common/common.hpp"

#include "quasar/algorithm/fefitwin/FeFitWin.hpp"
#include "quasar/algorithm/FeTemplate.hpp"
#include "quasar/algorithm/spectools/SpecTools.hpp"
#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"

#include "quasar/algorithm/utils.hpp"
#include "quasar/data/windows.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/Quasar/QuasarFactory.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_FEFITWIN
{
		SF_OCL_FEFITWIN() :
					quasar(QuasarFactory::createQuasarFromFit("test/test_data/test.fit"))
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

			namespace acl = algorithm::ocl;

			this->spec = std::vector<cl_float>(*(quasar.getData()));
			this->error = std::vector<cl_float>(*(quasar.getError()));
			this->feTemplate = common::file::loadFeTemplate("test/test_data/iron_emission_temp");
		}
		~SF_OCL_FEFITWIN()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;

		Quasar quasar;
		FeTemplate feTemplate;

		std::vector<cl_float> spec;
		std::vector<cl_float> error;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(FeFitWinTest, SF_OCL_FEFITWIN)

BOOST_AUTO_TEST_CASE(ConstructorTest)
{
	namespace acl = algorithm::ocl;
	BOOST_CHECK_NO_THROW(acl::FeFitWin(context, queue));
}

BOOST_AUTO_TEST_CASE(FeFitWinTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep device_time = 0;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	size_t size = 4096;
	// Okna żelaza
	std::vector<cl_float2> feWindows = data::fewinfull;

	// Macierze danych
	auto spectrumsMatrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto wavelengthsMatrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto errorsMatrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto continuumsMatrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer<cl_uint>(context, CL_MEM_READ_ONLY, size);

	std::vector<cl_uint> sizes_vec(size, ASTRO_OBJ_SPEC_SIZE);
	std::vector<cl_float> data_vec(size * ASTRO_OBJ_SPEC_SIZE, 1);

	acl::FeFitWin feFitWin(context, queue);
	acl::FeFitWin::Results result;

	copy_time += test::measure<>::execution([&]()
	{
		cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
		cl::copy(queue, data_vec.begin(), data_vec.end(), spectrumsMatrix);
		cl::copy(queue, data_vec.begin(), data_vec.end(), wavelengthsMatrix);
		cl::copy(queue, data_vec.begin(), data_vec.end(), errorsMatrix);
		cl::copy(queue, data_vec.begin(), data_vec.end(), continuumsMatrix);
	});

	device_time += test::measure<>::execution([&]()
	{
		BOOST_CHECK_NO_THROW(
			result = feFitWin.run(
					{ data_vec, data_vec, data_vec, data_vec, sizes_vec},
					{ spectrumsMatrix, wavelengthsMatrix, errorsMatrix, continuumsMatrix, sizes },
					size, feTemplate, feWindows)
		);
		queue.finish();
	});

	std::cout << "FeFitWinTestTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\trun(), liczba: " << size << ", czas: " << device_time << " ms" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
