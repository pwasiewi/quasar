#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_UTILS
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"

#include "quasar/algorithm/utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

struct SF_OCL_UTILS
{
		SF_OCL_UTILS()
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);
		}
		~SF_OCL_UTILS()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(UtilsTest, SF_OCL_UTILS)

/**
 *
 */
BOOST_AUTO_TEST_CASE(CopyToBufferTest)
{
	namespace au = algorithm::utils;

	auto quasars = test::quasar::generateQuasars(10, QUASAR_DATA_SIZE, false);

	// Ilość kwazarów
	size_t n = 10;
	// Rozmiar widma pojedynczego kwazara
	size_t dataSize = QUASAR_DATA_SIZE;
	// Rozmiar w bajtach
	size_t byteSize = n * dataSize * sizeof(Quasar::QFLOAT);
	cl::Buffer spec(context, CL_MEM_READ_WRITE, byteSize);
	cl::Buffer err(context, CL_MEM_READ_WRITE, byteSize);

	// Zapis do cl::Buffer
	BOOST_CHECK_NO_THROW(au::copy(queue, quasars, spec, err, nullptr));

	// Odczytam z cl::Buffer i sprawdzę, czy są dobre wartości.
	std::vector<cl_float> vtest(n * dataSize, 0);
	cl::copy(queue, spec, vtest.begin(), vtest.end());

	for (auto& i : vtest)
	{
		BOOST_CHECK_CLOSE(i, QUASAR_TEST_DATA_VALUE, 0.01);
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(CopyFromBufferTest)
{
	namespace au = algorithm::utils;

	size_t size = QUASAR_DATA_SIZE * 10;
	// Rozmiar w bajtach
	size_t byteSize = size * sizeof(Quasar::QFLOAT);
	// Wektor testowy
	std::vector<cl_float> in(size, QUASAR_TEST_DATA_VALUE);

	cl::Buffer buffer(context, CL_MEM_READ_WRITE, byteSize);
	// Kopiowanie
	// Host to Device
	cl::copy(queue, in.begin(), in.end(), buffer);

	// Wektor rozmiarów
	size_t N = 10;
	std::vector<cl_uint> sizes(N, QUASAR_DATA_SIZE);
	std::vector<std::vector<cl_float>> out;
	// Odczyt z cl::Buffer
	BOOST_CHECK_NO_THROW(out = au::copy_back<cl_float>(queue, buffer, sizes, nullptr));

	// Odpowiednia ilość wektorów
	BOOST_CHECK_EQUAL(out.size(), 10);
	// Sprawdzenie czy wartości zostały dobrze odczytane
	for (auto& vec : out)
	{
		for (auto& i : vec)
		{
			BOOST_CHECK_CLOSE(i, QUASAR_TEST_DATA_VALUE, 0.01);
		}
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(MakeBufferTest)
{
	namespace au = algorithm::utils;
	auto quasars = test::quasar::generateQuasars(10, 100, false);
	std::vector<cl_uint> s;
	BOOST_CHECK_NO_THROW(s = au::sizes(quasars));

	cl::Buffer spec;
	BOOST_CHECK_NO_THROW(spec = au::make_qbuffer(context, CL_MEM_READ_WRITE, s.size()));

	cl::Buffer err;
	BOOST_CHECK_NO_THROW(err = au::make_qbuffer(context, CL_MEM_READ_WRITE, s.size()));

	// Zapis do cl::Buffer
	BOOST_CHECK_NO_THROW(au::copy(queue, quasars, spec, err, nullptr));

	// Odczyt z cl::Buffer
	std::vector<std::vector<cl_float>> out;
	BOOST_CHECK_NO_THROW(out = au::copy_back<cl_float>(queue, spec, s, nullptr));

	// Odpowiednia ilość wektorów
	BOOST_CHECK_EQUAL(out.size(), 10);
	// Sprawdzenie czy wartości zostały dobrze odczytane
	for (auto& vec : out)
	{
		BOOST_CHECK_EQUAL(vec.size(), 100);
		for (auto& i : vec)
		{
			BOOST_CHECK_CLOSE(i, QUASAR_TEST_DATA_VALUE, 0.01);
		}
	}
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(SizesTest)
{
	namespace au = algorithm::utils;
	auto quasars = test::quasar::generateQuasars(10, 100, false);
	std::vector<cl_uint> s;
	BOOST_CHECK_NO_THROW(s = au::sizes(quasars));
	for (auto& i : s)
	{
		BOOST_CHECK_EQUAL(i, 100);
	}
}

BOOST_AUTO_TEST_SUITE_END()
