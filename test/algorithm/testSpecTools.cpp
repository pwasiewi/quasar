#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_SPECTOOLS
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/algorithm/spectools/SpecTools.hpp"
#include "quasar/algorithm/utils.hpp"

#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_SPECTOOLS
{
		SF_OCL_SPECTOOLS()
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

		}
		~SF_OCL_SPECTOOLS()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(SpecToolsTest, SF_OCL_SPECTOOLS)

std::vector<cl_float> host_gen_lambda(cl_float a, cl_float b, cl_float z, long size)
{
	std::vector<cl_float> lambda(size);

	long i = 0;
	std::generate(lambda.begin(), lambda.end(),
			[&]()
			{
				float t = (static_cast<cl_float>(i) * a) + b;
				t = pow10f(t);
				t /= (z + 1.);
				i++;
				return t;
			});
	return lambda;
}

std::vector<cl_float> host_addOne(
		std::vector<cl_float> lambdas, std::vector<cl_float> specs, std::vector<cl_uint> sizes,
		std::vector<cl_float> toAddLambda, std::vector<cl_float> toAdd)
{
	std::vector<cl_float> out(lambdas.size());

	auto sizesIter = sizes.begin();

	auto outStart = out.begin();

	auto lambdasIter = lambdas.begin();
	auto specIter = specs.begin();

	while (sizesIter != sizes.end())
	{
		auto toAddLambdaIter = toAddLambda.begin();
		auto toAddIter = toAdd.begin();
		for (unsigned int i = 0; i < *sizesIter;)
		{
			if (*lambdasIter >= *(toAddLambdaIter))
			{
				if (*lambdasIter > *(toAddLambdaIter + 1))
				{
					toAddLambdaIter++;
					toAddIter++;
				}
				else
				{
					cl_float a = (*(toAddIter + 1) - *toAddIter) / (*(toAddLambdaIter + 1) - *toAddLambdaIter);
					cl_float b = *toAddIter - (a * *toAddLambdaIter);
					*outStart = *specIter + (a * *lambdasIter + b);
					outStart++;

					lambdasIter++;
					specIter++;

					i++;
				}
			}
			else
			{
				*outStart = *specIter;
				outStart++;
				lambdasIter++;
				specIter++;

				i++;
			}

			if ((toAddLambdaIter + 1) == toAddLambda.end())
			{
				while (i < *sizesIter)
				{
					*outStart = *specIter;
					outStart++;
					lambdasIter++;
					specIter++;
					i++;
				}
			}
		}
		sizesIter++;
	}
	return out;
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(ConstructorTest)
{
	namespace acl = algorithm::ocl;

	BOOST_CHECK_NO_THROW(acl::SpecTools tools(context, queue));
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(GenerateWavelengthsMatrixTest)
{
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	long size = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	// Generacja współczynników a, b i z
	cl_float4 abz = { 0.0001, 3.5797, 1.768, 0 };
	std::vector<cl_float4> abz_(size, abz);

	// Kopiowanie na device
	auto abz_buffer = au::make_buffer(context, CL_MEM_READ_ONLY, abz_);
	cl::copy(queue, abz_.begin(), abz_.end(), abz_buffer);

	// Bufor cl::Buffer na lambdy
	auto lambdas = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);

	std::shared_ptr<acl::SpecTools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::SpecTools(context, queue)));

	int max_i = 5;
	for (int i = 0; i < max_i; i++)
	{
		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(tools->generateWavelengthsMatrix(abz_buffer, size, lambdas, nullptr, &event));
		event.wait();

		// Wczytuje jedną lambdę z device do host dla późniejszego porównania.
		std::vector<cl_float> lambda(QUASAR_DATA_SIZE, 0);
		queue.finish();
		cl::copy(queue, lambdas, lambda.begin(), lambda.end());
		queue.finish();

		// Generacja na host
		std::vector<cl_float> host_lambda;
		host_time += test::measure<>::execution([&]()
		{
			host_lambda = host_gen_lambda(abz.s[0], abz.s[1], abz.s[2], size*QUASAR_DATA_SIZE);
		});

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_lambda.begin(), host_lambda.begin()+QUASAR_DATA_SIZE,
				lambda.begin(), lambda.end(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	host_time /= max_i;

	std::cout << "GenerateLambdasTest:" << std::endl;
	std::cout << "\tgenerateLambdas(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_generate_lambda(), liczba: " << size << ", czas: " << host_time << " ms" << std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(AddSpectrumTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	double device_time = 0;

	long size = 4000;
	long to_add_size = 4200;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> specs_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_float> specs_lamba_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> to_add_vec(to_add_size, 1);
	std::vector<cl_float> to_add_lamba_vec(to_add_size, 1);
	std::vector<cl_uint> sizes_vec(size, QUASAR_DATA_SIZE);

	cl_float i = 1070.240;
	std::generate(to_add_lamba_vec.begin(), to_add_lamba_vec.end(),
			[&]()
			{
				i+=0.1;
				return i;
			});

	i = 1370.0;
	std::generate(specs_lamba_vec.begin(), specs_lamba_vec.end(),
			[&]()
			{
				i += 0.44;
				return i;
			});

	i = 1.0;
	std::generate(to_add_vec.begin(), to_add_vec.end(),
			[&]()
			{
				i += 0.433333 + (static_cast<float>(rand())/static_cast<float>(RAND_MAX));;
				return i;
			});

	// Bufory
	auto specs_trans = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto temp = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto lambdas_trans = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto out = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto to_add = au::make_buffer<cl_float>(context, CL_MEM_READ_ONLY, to_add_size);
	auto to_add_lambda = au::make_buffer<cl_float>(context, CL_MEM_READ_ONLY, to_add_size);

	std::vector<cl_float> host_added;
	auto host_time = test::measure<>::execution([&]()
	{
		host_added = host_addOne(specs_lamba_vec, specs_vec, sizes_vec,
				to_add_lamba_vec, to_add_vec);
	});

	std::shared_ptr<acl::SpecTools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::SpecTools(context, queue)));

	acl::BasicMatrix bm(context, queue);

	int max_i = 2;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, specs_vec.begin(), specs_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, specs_trans);

			cl::copy(queue, specs_lamba_vec.begin(), specs_lamba_vec.end(), temp);
			bm.transpose(temp, QUASAR_DATA_SIZE, size, lambdas_trans);

			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, to_add_vec.begin(), to_add_vec.end(), to_add);
			cl::copy(queue, to_add_lamba_vec.begin(), to_add_lamba_vec.end(), to_add_lambda);
		});

		//
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->addSpectrum(specs_trans, lambdas_trans, sizes, size, to_add_lambda, to_add, to_add_size, out, nullptr, &event));
		event.wait();

		bm.transpose(out, size, QUASAR_DATA_SIZE, temp);

		// Wczytuje jedno widmo po obliczeniach
		std::vector<cl_float> added(QUASAR_DATA_SIZE, 0);
		queue.finish();
		cl::copy(queue, temp, added.begin(), added.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_added.begin(), host_added.begin() + 300,
				added.begin(), added.begin() + 300,
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "AddSpectrumTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\taddSpectrum(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_addSpectrum(), liczba: " << size << ", czas: " << host_time << " ms" << std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(FilterWithWavelengthWindowsTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	double device_time = 0;

	long size = 4096;
	long windows_size = 20;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> specs_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_float> specs_lamba_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> errors_vec(size * QUASAR_DATA_SIZE, 0.01);
	std::vector<cl_uint> sizes_vec(size, QUASAR_DATA_SIZE);
	std::vector<cl_float2> windows_vec(windows_size);

	cl_float2 i2 = { 800.0, 840 };
	std::generate(windows_vec.begin(), windows_vec.end(),
			[&]()
			{
				i2.s[0] += 200;
				i2.s[1] += 200;
				return i2;
			});

	float i = 999.0;
	std::generate(specs_lamba_vec.begin(), specs_lamba_vec.end(),
			[&]()
			{
				i += 10.0;
				return i;
			});

	// Bufory
	auto specs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto lambdas = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto errors = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);
	auto windows = au::make_buffer<cl_float2>(context, CL_MEM_READ_ONLY, windows_size);

	std::shared_ptr<acl::SpecTools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::SpecTools(context, queue)));

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, specs_vec.begin(), specs_vec.end(), specs);
			cl::copy(queue, errors_vec.begin(), errors_vec.end(), errors);
			cl::copy(queue, specs_lamba_vec.begin(), specs_lamba_vec.end(), lambdas);
			cl::copy(queue, windows_vec.begin(), windows_vec.end(), windows);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->filterWithWavelengthWindows(specs, lambdas, errors, sizes, size, windows, windows_size, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		std::vector<cl_float> filtered(QUASAR_DATA_SIZE, 0);
		queue.finish();
		cl::copy(queue, lambdas, filtered.begin(), filtered.end());
		queue.finish();

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "FilterWithWavelengthWindowsTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tfilterWithWavelengthWindows(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
}

BOOST_AUTO_TEST_CASE(FilterNonpositiveTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	double device_time = 0;

	long size = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> specs_vec(size * QUASAR_DATA_SIZE, 3);
	std::vector<cl_float> specs_lamba_vec(size * QUASAR_DATA_SIZE, 1);
	std::vector<cl_float> errors_vec(size * QUASAR_DATA_SIZE, 0.01);
	std::vector<cl_uint> sizes_vec(size, QUASAR_DATA_SIZE);

	int i = 0;
	std::generate(specs_vec.begin(), specs_vec.end(),
			[&]()
			{
				i++;
				if(i % 2 == 0)
				{
					return -1.0f;
				}
				return static_cast<float>(i);
			});

	// Bufory
	auto specs = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto lambdas = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto errors = au::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	auto sizes = au::make_buffer(context, CL_MEM_READ_ONLY, sizes_vec);

	std::shared_ptr<acl::SpecTools> tools;
	BOOST_CHECK_NO_THROW(tools.reset(new acl::SpecTools(context, queue)));

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, sizes_vec.begin(), sizes_vec.end(), sizes);
			cl::copy(queue, specs_vec.begin(), specs_vec.end(), specs);
			cl::copy(queue, errors_vec.begin(), errors_vec.end(), errors);
			cl::copy(queue, specs_lamba_vec.begin(), specs_lamba_vec.end(), lambdas);
		});

		// Generacja na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(
				tools->filterNonpositive(specs, lambdas, errors, sizes, size, nullptr, &event));
		event.wait();

		// Wczytuje po obliczeniach
		std::vector<cl_float> filtered(size * QUASAR_DATA_SIZE, -1);
		queue.finish();
		cl::copy(queue, specs, filtered.begin(), filtered.end());
		queue.finish();

		for(auto i : filtered)
		{
			BOOST_CHECK_GT(i, 0.0f);
		}

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "FilterNonpositiveTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tfilterNonpositive(), liczba: " << size << ", czas: " << device_time / 1000000 << " ms" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
