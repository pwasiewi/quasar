#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_OCL_BASICMATRIX
#endif
#include <boost/test/unit_test.hpp>

#include "../test_defines.hpp"
#include "../test_quasar.hpp"
#include "../test_macros.hpp"

#include "quasar/algorithm/basicmatrix/BasicMatrix.hpp"
#include "quasar/algorithm/utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <memory>
#include <algorithm>

struct SF_OCL_BASICMATRIX
{
		SF_OCL_BASICMATRIX()
		{
			std::vector<cl::Platform> platforms;
			cl::Platform::get(&platforms);

			if (platforms.empty())
			{
				throw std::runtime_error("No OpenCL platform");
			}

			this->platform = platforms[0];

			std::vector<cl::Device> devices;
			this->platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
			if (devices.empty())
			{
				throw std::runtime_error("No OpenCL GPU devices");
			}

			this->context = cl::Context(devices);
			this->queue = cl::CommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE);

		}
		~SF_OCL_BASICMATRIX()
		{
			queue.flush();
			queue.finish();
		}

		cl::Context context;
		cl::Platform platform;
		cl::CommandQueue queue;
};

/**
 *
 */
BOOST_FIXTURE_TEST_SUITE(BasicMatrixTest, SF_OCL_BASICMATRIX)

/**
 *
 */
BOOST_AUTO_TEST_CASE(ConstructorTest)
{
	namespace acl = algorithm::ocl;
	BOOST_CHECK_NO_THROW(acl::BasicMatrix bm(context, queue));
}

BOOST_AUTO_TEST_CASE(Log10Test)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t rows = 4096;
	size_t row_size = QUASAR_DATA_SIZE;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(rows * row_size, 1);

	int i = 0;
	std::generate(data_vec.begin(), data_vec.end(),
			[&]()
			{
				i++;
				if(i % 24 == 0 )
				{
					return -1.f;
				}
				return static_cast<float>(i);
			});

	// Bufory
	auto data = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);

	std::shared_ptr<acl::BasicMatrix> bm;
	BOOST_CHECK_NO_THROW(bm.reset(new acl::BasicMatrix(context, queue)));

	// Obliczenia na host
	std::vector<cl_float> host_data_vec(rows * row_size, 0);
	host_time += test::measure<>::execution([&]()
	{
		for(size_t i = 0; i < rows; i++)
		{
			size_t idx = i * row_size;
			for(size_t j = 0; j < row_size; j++)
			{
				float v = data_vec[idx];
				if(v < 0)
				{
					host_data_vec[idx] = NAN;
				}
				else
				{
					host_data_vec[idx] = std::log10(v);
				}
				idx++;
			}
		}
	});

	int max_i = 5;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), data);
		});

		// Obliczenia na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(bm->log10(data, row_size, rows, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, data, data_vec.begin(), data_vec.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_data_vec.begin(), host_data_vec.begin(),
				data_vec.begin(), data_vec.begin(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "Log10Test:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tlog10(), macierz: " << rows << " x " << row_size
			<< ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_log10(), macierz: " << rows << " x " << row_size
			<< ", czas: " << host_time << " ms"	<< std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(MinusScalarTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t rows = 4096;
	size_t row_size = ASTRO_OBJ_SPEC_SIZE;

	cl_float subtrahend = 2.0f;
	cl_float minuend = 4.0f;
	cl_float difference = minuend - subtrahend;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(rows * row_size, minuend);

	// Bufory
	auto data = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);

	std::shared_ptr<acl::BasicMatrix> bm;
	BOOST_CHECK_NO_THROW(bm.reset(new acl::BasicMatrix(context, queue)));

	// Obliczenia na host
	std::vector<cl_float> host_data_vec(rows * row_size, 0);
	host_time += test::measure<>::execution([&]()
	{
		for(size_t i = 0; i < rows; i++)
		{
			size_t idx = i * row_size;
			for(size_t j = 0; j < row_size; j++)
			{
				host_data_vec[idx] =  data_vec[idx] - subtrahend;
				idx++;
			}
		}
	});

	// Sprawdzenie poprawności hosta
	for(auto i : host_data_vec)
	{
		BOOST_CHECK_CLOSE(i, difference, 0.1f);
	}

	int max_i = 5;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), data);
		});

		// Obliczenia na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(bm->minus(data, row_size, rows, subtrahend, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, data, data_vec.begin(), data_vec.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_data_vec.begin(), host_data_vec.begin(),
				data_vec.begin(), data_vec.begin(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "MinusScalarTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tminus(), macierz: " << rows << " x " << row_size
			<< ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_minus(), macierz: " << rows << " x " << row_size
			<< ", czas: " << host_time << " ms"	<< std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(TransposeTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t height = 4097;
	size_t width = 4096;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(height  * width, 1);

	int i = 0;
	std::generate(data_vec.begin(), data_vec.end(),
			[&]()
			{
				i++;
				return static_cast<float>(i);
			});

	// Bufory
	auto matrix = au::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, height * width);
	auto tmatrix = au::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, height * width);

	std::shared_ptr<acl::BasicMatrix> bm;
	BOOST_CHECK_NO_THROW(bm.reset(new acl::BasicMatrix(context, queue)));

	// Obliczenia na host
	std::vector<cl_float> host_data_vec(height * width, 0);
	host_time += test::measure<>::execution([&]()
	{
		for(size_t i = 0; i < height; i++)
		{
			for(size_t j = 0; j < width; j++)
			{
				host_data_vec[j * height + i] =  data_vec[i * width + j];
			}
		}
	});

	int max_i = 1;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), matrix);
		});

		// Obliczenia na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(bm->transpose(matrix, width, height, tmatrix, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, tmatrix, data_vec.begin(), data_vec.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_data_vec.begin(), host_data_vec.begin(),
				data_vec.begin(), data_vec.begin(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "TransposeTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\ttranspose(), macierz: " << width << " x " << height
			<< ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_transpose(), macierz: " << width << " x " << height
			<< ", czas: " << host_time << " ms"	<< std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(MinusMatrixTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t rows = 4096;
	size_t row_size = ASTRO_OBJ_SPEC_SIZE;

	cl_float subtrahend = 2.0f;
	cl_float minuend = 4.0f;
	cl_float difference = minuend - subtrahend;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(rows * row_size, minuend);
	std::vector<cl_float> subtrahend_vec(rows * row_size, subtrahend);

	// Bufory
	auto data = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);
	auto subtrahend_matrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);

	std::shared_ptr<acl::BasicMatrix> bm;
	BOOST_CHECK_NO_THROW(bm.reset(new acl::BasicMatrix(context, queue)));

	// Obliczenia na host
	std::vector<cl_float> host_data_vec(rows * row_size, 0);
	host_time += test::measure<>::execution([&]()
	{
		for(size_t i = 0; i < rows; i++)
		{
			size_t idx = i * row_size;
			for(size_t j = 0; j < row_size; j++)
			{
				host_data_vec[idx] =  data_vec[idx] - subtrahend_vec[idx];
				idx++;
			}
		}
	});

	// Sprawdzenie poprawności hosta
	for(auto i : host_data_vec)
	{
		BOOST_CHECK_CLOSE(i, difference, 0.1f);
	}

	int max_i = 5;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), data);
			cl::copy(queue, subtrahend_vec.begin(), subtrahend_vec.end(), subtrahend_matrix);
		});

		// Obliczenia na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(bm->minus(data, row_size, rows, subtrahend_matrix, data, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, data, data_vec.begin(), data_vec.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_data_vec.begin(), host_data_vec.begin(),
				data_vec.begin(), data_vec.begin(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "MinusMatrixTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tminus(), macierz: " << rows << " x " << row_size
			<< ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_minus(), macierz: " << rows << " x " << row_size
			<< ", czas: " << host_time << " ms"	<< std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(MultiplyColTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t rows = 4096;
	size_t row_size = ASTRO_OBJ_SPEC_SIZE;

	cl_float multi = 2.0f;
	cl_float value = 4.0f;
	cl_float result = multi * value;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> data_vec(rows * row_size, value);
	std::vector<cl_float> multi_vec(row_size, multi);

	// Bufory
	auto data = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);
	auto vector = au::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, row_size);

	std::shared_ptr<acl::BasicMatrix> bm;
	BOOST_CHECK_NO_THROW(bm.reset(new acl::BasicMatrix(context, queue)));

	// Obliczenia na host
	std::vector<cl_float> host_data_vec(rows * row_size, 0);
	host_time += test::measure<>::execution([&]()
	{
		for(size_t i = 0; i < rows; i++)
		{
			size_t idx = i * row_size;
			for(size_t j = 0; j < row_size; j++)
			{
				host_data_vec[idx] =  data_vec[idx] * multi_vec[j];
				idx++;
			}
		}
	});

	// Sprawdzenie poprawności hosta
	for(auto i : host_data_vec)
	{
		BOOST_CHECK_CLOSE(i, result, 0.1f);
	}

	int max_i = 5;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, data_vec.begin(), data_vec.end(), data);
			cl::copy(queue, multi_vec.begin(), multi_vec.end(), vector);
		});

		// Obliczenia na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(bm->multiplyCol(data, row_size, rows, vector, data, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, data, data_vec.begin(), data_vec.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_data_vec.begin(), host_data_vec.begin(),
				data_vec.begin(), data_vec.begin(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "MultiplyColTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tmultiplyCol(), macierz: " << rows << " x " << row_size
			<< ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_multiplyCol(), macierz: " << rows << " x " << row_size
			<< ", czas: " << host_time << " ms"	<< std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_CASE(DivideMatrixTest)
{
	std::chrono::milliseconds::rep copy_time = 0;
	std::chrono::milliseconds::rep host_time = 0;
	double device_time = 0;

	size_t rows = 4096;
	size_t row_size = ASTRO_OBJ_SPEC_SIZE;

	cl_float divisor = 2.0f;
	cl_float dividend = 4.0f;
	cl_float quotient = dividend / divisor;

	namespace au = algorithm::utils;
	namespace acl = algorithm::ocl;

	std::vector<cl_float> dividend_vec(rows * row_size, dividend);
	std::vector<cl_float> divisor_vec(rows * row_size, divisor);

	// Bufory
	auto divident_matrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);
	auto divisor_matrix = au::make_qbuffer(context, CL_MEM_READ_WRITE, rows);

	std::shared_ptr<acl::BasicMatrix> bm;
	BOOST_CHECK_NO_THROW(bm.reset(new acl::BasicMatrix(context, queue)));

	// Obliczenia na host
	std::vector<cl_float> host_data_vec(rows * row_size, 0);
	host_time += test::measure<>::execution([&]()
	{
		for(size_t i = 0; i < rows; i++)
		{
			size_t idx = i * row_size;
			for(size_t j = 0; j < row_size; j++)
			{
				host_data_vec[idx] =  dividend_vec[idx] / divisor_vec[idx];
				idx++;
			}
		}
	});

	// Sprawdzenie poprawności hosta
	for(auto i : host_data_vec)
	{
		BOOST_CHECK_CLOSE(i, quotient, 0.1f);
	}

	int max_i = 5;
	for (int i = 0; i < max_i; i++)
	{
		copy_time += test::measure<>::execution([&]()
		{
			cl::copy(queue, dividend_vec.begin(), dividend_vec.end(), divident_matrix);
			cl::copy(queue, divisor_vec.begin(), divisor_vec.end(), divisor_matrix);
		});

		// Obliczenia na device
		cl::Event event;
		BOOST_CHECK_NO_THROW(bm->divide(divident_matrix, row_size, rows, divisor_matrix, divident_matrix, nullptr, &event));
		event.wait();

		// Wczytuje jedno widmo po obliczeniach
		queue.finish();
		cl::copy(queue, divident_matrix, dividend_vec.begin(), dividend_vec.end());
		queue.finish();

		// Porównanie wyników host i device
		TEST_CLOSE_COLLECTION(
				host_data_vec.begin(), host_data_vec.begin(),
				dividend_vec.begin(), dividend_vec.begin(),
				0.1);

		// Czas wykonania
		double time = static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_END>());
		time -= static_cast<double>(event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
		device_time += time;
	}

	device_time /= static_cast<double>(max_i);
	copy_time /= max_i;

	std::cout << "DivideMatrixTest:" << std::endl;
	std::cout << "\tcopy_time, czas: " << copy_time << " ms" << std::endl;
	std::cout << "\tdivide(), macierz: " << rows << " x " << row_size
			<< ", czas: " << device_time / 1000000 << " ms" << std::endl;
	std::cout << "\thost_divide(), macierz: " << rows << " x " << row_size
			<< ", czas: " << host_time << " ms"	<< std::endl;
	std::cout << "\tZysk: " << (static_cast<double>(host_time) / (device_time / 1000000)) * 100 << "%" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
