#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_QUASAR_FACTORY
#endif
#include <boost/test/unit_test.hpp>

#include "test_defines.hpp"
#include "quasar/Quasar/QuasarFactory.hpp"

#include <boost/filesystem/path.hpp>

#include <cmath>

/**
 * Test fabryki kwazarów.
 * @see QuasarFactory
 */
BOOST_AUTO_TEST_SUITE(QuasarFactoryTest)

const std::string fitPath("test/test_data/test.fit");

/**
 *
 */
BOOST_AUTO_TEST_CASE(CreateQuasarFromFitTest)
{
	auto q = QuasarFactory::createQuasarFromFit(fitPath);

	BOOST_CHECK(q.getData() != nullptr);
	BOOST_CHECK(q.getDataSize() > 0);
	BOOST_CHECK(q.getParams() != nullptr);
	BOOST_CHECK(q.getError() != nullptr);
	BOOST_CHECK(!q.getParams()->name.empty());
	BOOST_CHECK(!q.getParams()->type.empty());
}

/**
 * Czy wczytują się parametry z pliku fits.
 */
BOOST_AUTO_TEST_CASE(LoadFitQuasarParams)
{
	std::shared_ptr<CCfits::FITS> file(new CCfits::FITS(fitPath, CCfits::Read));
	Quasar::qparams * p = nullptr;
	BOOST_CHECK_NO_THROW(p = QuasarFactory::_getQuasarParamsFromFit(file));

	BOOST_CHECK(!p->name.empty());
	BOOST_CHECK(p->type.compare(QUASAR_TEST_FIT_TYPE) == 0);
	BOOST_CHECK(fabs(p->a) > 0);
	BOOST_CHECK(fabs(p->b) > 0);
	BOOST_CHECK(p->mjd > 0);
	BOOST_CHECK(fabs(p->ra) > 0);
	BOOST_CHECK(fabs(p->dec) > 0);
	BOOST_CHECK(p->plate > 0);
	BOOST_CHECK(p->fiber > 0);

	delete p;
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(LoadFitQuasarData)
{
	std::shared_ptr<CCfits::FITS> file(new CCfits::FITS(fitPath, CCfits::Read));
	unsigned long n(file->pHDU().axis(0));

	Quasar::qdata * d = nullptr;
	BOOST_CHECK_NO_THROW(d = QuasarFactory::_getQuasarDataFromFit(file));
	BOOST_CHECK_EQUAL(n, d->size());

	delete d;
}

/**
 *
 */
BOOST_AUTO_TEST_CASE(LoadFitQuasarErr)
{
	std::shared_ptr<CCfits::FITS> file(new CCfits::FITS(fitPath, CCfits::Read));
	unsigned long n(file->pHDU().axis(0));

	Quasar::qdata * e = nullptr;
	BOOST_CHECK_NO_THROW(e = QuasarFactory::_getQuasarErrFromFit(file));
	BOOST_CHECK_EQUAL(n, e->size());

	delete e;
}

BOOST_AUTO_TEST_SUITE_END()
