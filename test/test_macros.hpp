#pragma once

#include <boost/test/unit_test.hpp>
#include <boost/limits.hpp>  // for std::numeric_limits
#include <boost/numeric/conversion/conversion_traits.hpp> // for numeric::conversion_traits

#include <type_traits>
#include <cmath>

#define TOKEN_PASTEx(x, y) x ## y
#define TOKEN_PASTE(x, y) TOKEN_PASTEx(x, y)

#define TEST_CLOSE_COLLECTION( L_begin, L_end, R_begin, R_end, T ) \
		TEST_CLOSE_COLLECTION_INTERNAL( L_begin, L_end, R_begin, R_end, T, __COUNTER__ )

#define TEST_CLOSE_COLLECTION_INTERNAL( L_begin, L_end, R_begin, R_end, T, counter ) \
	 	auto TOKEN_PASTE(auto_tcc_, counter) = \
		::boost::test_tools::tt_detail::close_coll_impl( (L_begin), (L_end), (R_begin), (R_end), (::boost::test_tools::percent_tolerance(T)) ); \
		if (!TOKEN_PASTE(auto_tcc_, counter)) \
		{ \
			BOOST_FAIL(TOKEN_PASTE(auto_tcc_, counter).message().str()); \
		} \

namespace boost
{

namespace test_tools
{

namespace tt_detail
{

//____________________________________________________________________________//

template<typename Left, typename Right, typename ToleranceBaseType>
inline predicate_result
close_coll_impl(Left left_begin, Left left_end, Right right_begin, Right right_end, ::boost::test_tools::percent_tolerance_t<ToleranceBaseType> tolerance,
		floating_point_comparison_type fpc_type = FPC_STRONG)
{
	predicate_result res(true);
	std::size_t pos = 0;

	typedef typename std::remove_reference<decltype(*left_begin)>::type FPT1;
	typedef typename std::remove_reference<decltype(*right_begin)>::type FPT2;
	// deduce "better" type from types of arguments being compared
	// if one type is floating and the second integral we use floating type and
	// value of integral type is promoted to the floating. The same for float and double
	// But we don't want to compare two values of integral types using this tool.
	typedef typename ::boost::numeric::conversion_traits<FPT1, FPT2>::supertype FPT;
	BOOST_STATIC_ASSERT(!::boost::is_integral<FPT>::value);

	close_at_tolerance<FPT> pred(tolerance, fpc_type);

	for (; left_begin != left_end && right_begin != right_end; ++left_begin, ++right_begin, ++pos)
	{
		// NAN fixed
		if (!pred(*left_begin, *right_begin) && !(std::isnan(*left_begin) && std::isnan(*right_begin)))
		{
			if(!(std::isnan(*left_begin) && std::isnan(*right_begin)))
			{
				std::cout << "s";
			}
			res = false;
			res.message() << "\nMismatch in a position " << pos << ": " << *left_begin << " != " << *right_begin;
		}
	}

	if (left_begin != left_end)
	{
		std::size_t r_size = pos;
		while (left_begin != left_end)
		{
			++pos;
			++left_begin;
		}

		res = false;
		res.message() << "\nCollections size mismatch: " << pos << " != " << r_size;
	}

	if (right_begin != right_end)
	{
		std::size_t l_size = pos;
		while (right_begin != right_end)
		{
			++pos;
			++right_begin;
		}

		res = false;
		res.message() << "\nCollections size mismatch: " << l_size << " != " << pos;
	}

	return res;
}

//____________________________________________________________________________//

} /* tt_detail */

} /* test_tools */

} /* boost */


