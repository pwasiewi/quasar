#include "db.hpp"

namespace db
{

std::shared_ptr<Connection> buildConnection(const connectionParams& params, const DBType databaseType)
{
	switch (databaseType)
	{
		case DBType::MONGODB:
			return std::shared_ptr<Connection>(new db_mongo::Connection(params));
			break;
	}
	return nullptr;
}

std::shared_ptr<QuasarSet> buildQuasarSet(const std::string& name, const std::time_t date, const std::time_t insertDate,
		const long long size, const DBType databaseType)

{
	switch (databaseType)
	{
		case DBType::MONGODB:
			return std::shared_ptr<QuasarSet>(new db_mongo::QuasarSet(name, date, insertDate, size));
			break;
	}
	return nullptr;
}

} /* namespace DB */
