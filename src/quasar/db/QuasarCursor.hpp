#pragma once

#include "quasar/Quasar/Quasar.hpp"

#include <memory>
#include <vector>

namespace db
{

/**
 *
 */
class QuasarCursor
{
	public:
		virtual ~QuasarCursor();
		virtual std::vector<Quasar> next(const size_t) = 0;
		virtual bool more() = 0;
};

} /* namespace DB */
