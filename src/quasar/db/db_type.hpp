#pragma once

namespace db
{

/**
 * Dostępne bazy danych.
 */
enum class DBType
{
	MONGODB //!< Baza danych mongodb
};

}
