#pragma once

#include "db_type.hpp"
#include "Connection.hpp"
#include "QuasarSet.hpp"
#include "QuasarCursor.hpp"

#include "mongo/Connection.hpp"
#include "mongo/QuasarSet.hpp"
#include "mongo/QuasarCursor.hpp"

#include <ctime>

namespace db
{

std::shared_ptr<Connection> buildConnection(const connectionParams&, const DBType databaseType = DBType::MONGODB);
std::shared_ptr<QuasarSet> buildQuasarSet(const std::string&, const std::time_t,
		const std::time_t insertDate = std::time(nullptr), const long long size = 0,
		const DBType databaseType = DBType::MONGODB);

} /* namespace DB */
