#include "ParameterizationResultSet.hpp"

namespace db
{

ParameterizationResultSet::ParameterizationResultSet(const std::string& name, const std::time_t date,
		const ParameterizationOptions options, const std::string& description) :
			name(name),
			date(date),
			options(options),
			description(description)
{

}

ParameterizationResultSet::~ParameterizationResultSet()
{

}

} /* namespace db */
