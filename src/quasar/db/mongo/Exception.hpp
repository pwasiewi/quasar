#pragma once

#include "../Exception.hpp"

namespace db_mongo
{

class OIDAlreadySet: public db::LogicError
{
	public:
		OIDAlreadySet();
		virtual ~OIDAlreadySet();
};

class OIDInvalid: public db::RuntimeError
{
	public:
		OIDInvalid();
		virtual ~OIDInvalid();
};

} /* namespace db_mongo */
