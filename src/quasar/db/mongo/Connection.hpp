#pragma once

#include "db_mongo.hpp"
#include "QuasarSet.hpp"
#include "ParameterizationResultSet.hpp"

#include "../Connection.hpp"

#include <memory>
#include <string>
#include <vector>

namespace db_mongo
{

bool isInitialized();

class Connection: public db::Connection
{
	public:
		Connection(const db::connectionParams&);
		Connection(const std::string& host, const int port, const std::string& database);
		~Connection();

		// ************************
		// Metody wirtualne z DB:Connection
		// ************************
		void connect();
		void saveQuasarSet(std::shared_ptr<db::QuasarSet>);
		void deleteQuasarSet(std::shared_ptr<db::QuasarSet>);
		std::shared_ptr<db::QuasarSet> getQuasarSetByName(const std::string&);
		std::unique_ptr<db::Connection::QuasarSets> getQuasarSets();
		std::shared_ptr<db::QuasarSet> getNewQuasarSet(const std::string&, const std::time_t,
				const std::time_t insertDate = std::time(nullptr), const size_t size = 0) const;

		void saveParaResultSet(std::shared_ptr<db::ParameterizationResultSet> prs);
		std::shared_ptr<db::ParameterizationResultSet> getNewParaResultSet(const std::string& name,
				 const std::time_t date, db::ParameterizationResultSet::ParameterizationOptions options,
				 const std::string& description = "") const;

		db::DBType getDBType() const;
		bool isConnected() const;

		// ************************
		// Dodatkowe metody tylko dla MongoDB::QuasarSet
		// ************************
		std::shared_ptr<::mongo::DBClientConnection> getMongoConnection() const;

	private:
		/**
		 * Połączenie z MongoDB.
		 */
		std::shared_ptr<::mongo::DBClientConnection> connection;
		/**
		 *
		 */
		void init();
		/**
		 * Ustawia pustą bazę danych, dodaje odpowiednie indeksy.
		 */
		void setupDatabase();
};
}
/* namespace db_mongo */
