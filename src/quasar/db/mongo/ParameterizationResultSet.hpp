#pragma once

#include "db_mongo.hpp"
#include "common.hpp"
#include "bson_helper.hpp"

#include "../ParameterizationResultSet.hpp"
#include "../Exception.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/algorithm/specparameterization/SpecParameterization.hpp"

#include <vector>
#include <ctime>

namespace db_mongo
{

class ParameterizationResultSet: public db::ParameterizationResultSet
{
		friend class db_mongo::Connection;

	public:
		ParameterizationResultSet(const std::string& name, const std::time_t date,
				const ParameterizationOptions options, const std::string& description = "");
		~ParameterizationResultSet();

		void saveResults(std::vector<Quasar> astroObjects, ParaResults results);
		mongo::BSONObj getBSONObj() const;
		const mongo::OID& getOID() const;

	private:
		void setOID(const mongo::OID oid);
		void setDatabase(const std::string& database);
		void setConnection(const std::shared_ptr<mongo::DBClientConnection> connection);

		/**
		 * Dostępne tylko podczas załadowania QuasarSet z bazy danych, lub po zapisaniu do niej.
		 */
		mongo::OID oid;

		/**
		 * Baza danych
		 */
		std::string database;

		/**
		 * Połączenie z MongoDB.
		 */
		std::shared_ptr<mongo::DBClientConnection> connection;
};

} /* namespace db */
