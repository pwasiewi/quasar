#include "ParameterizationResultSet.hpp"

namespace db_mongo
{

ParameterizationResultSet::ParameterizationResultSet(const std::string& name, const std::time_t date,
		const ParameterizationOptions options, const std::string& description) :
			db::ParameterizationResultSet(name, date, options, description)
{

}

ParameterizationResultSet::~ParameterizationResultSet()
{

}

void ParameterizationResultSet::saveResults(std::vector<Quasar> astroObjects, ParaResults results)
{
	std::string ns = db_mongo::common::namespaceBuilder({ this->database, _para_result_col_name() });

	for (size_t i = 0; i < astroObjects.size(); i++)
	{
		mongo::BSONObjBuilder result;
		result.genOID();
		result.append("para_result_set_oid", getOID());
		result.append("mjd", astroObjects[i].getParams()->mjd);
		result.append("plate", astroObjects[i].getParams()->plate);
		result.append("fiber", astroObjects[i].getParams()->fiber);

		result.append("cont_chisq", static_cast<double>(results.chisqs[i]));
		result.appendArray("cont_reglin_result", common::arrayToBSONObj(results.continuum_reglin_results[i].s, 5));
		result.appendArray("reglin_result", common::arrayToBSONObj(results.reglin_results[i].s, 5));

		result.append("fe_scaleRate", static_cast<double>(results.scaleRates[i]));
		result.append("fe_wins_size", static_cast<unsigned int>(results.sizes_fewindows[i]));
		result.append("fe_wins_reduced_chisq", static_cast<double>(results.reducedChisqs_fewindows[i]));

		result.append("fe_full_reduced_chisq", static_cast<double>(results.reducedChisqs_full[i]));
		result.append("fe_full_ew", static_cast<double>(results.ews_full[i]));

		result.append("fe_range_reduced_chisq", static_cast<double>(results.reducedChisqs_feRange[i]));
		result.append("fe_range_ew", static_cast<double>(results.ews_feRange[i]));
		{
			mongo::BSONArrayBuilder elements_fits;
			for (size_t j = 0; j < options.elements.size(); j++)
			{
				// Jeżeli udało się dopasować.
				if(results.elementsFits[j].success(i))
				{
					auto& e = results.elementsFits[j];
					mongo::BSONObjBuilder element;
					element.append("name", options.elements[j].name);
					element.append("gaussian_params",
										BSON_ARRAY(
												static_cast<double>(e.fitParams[i].s[0])
												<< static_cast<double>(e.fitParams[i].s[1])
												<< static_cast<double>(e.fitParams[i].s[2]) ));
					element.append("gaussian_fwhm", static_cast<double>(e.gaussian_fwhms[i]));
					element.append("chisq", static_cast<double>(e.chisqs[i]));
					element.append("ew", static_cast<double>(e.ews[i]));
					elements_fits.append(element.obj());
				}
			}
			result.appendArray("elements_fits", elements_fits.obj());
		}
		this->connection->insert(ns, result.obj());
	}
}

/**
 * Zwraca obiekt BSONObj opisujący ten wyniki;
 * @return obiekt BSONObj.
 */
mongo::BSONObj ParameterizationResultSet::getBSONObj() const
{
	using namespace mongo;
	BSONObjBuilder bob;
	if (!this->oid.isSet())
	{
		bob.genOID();
	}
	else
	{
		bob.append("_id", this->oid);
	}
	bob.append("name", this->name);
	bob.append("set_name", this->options.setName);
	bob.append("date", common::to_Date_t(date));
	bob.append("size", static_cast<long long>(this->options.size));
	bob.append("options", common::paraOptionsToBSONObj(options));
	bob.append("description", this->description);
	return bob.obj();
}

const mongo::OID& ParameterizationResultSet::getOID() const
{
	if (!this->oid.isSet())
	{
		throw OIDInvalid();
	}
	return this->oid;
}

void ParameterizationResultSet::setOID(const mongo::OID oid)
{
	if (!oid.isSet())
	{
		throw OIDInvalid();
	}
	this->oid = oid;
}

void ParameterizationResultSet::setDatabase(const std::string& database)
{
	if (this->database.empty())
	{
		this->database = database;
	}
}

void ParameterizationResultSet::setConnection(const std::shared_ptr<mongo::DBClientConnection> connection)
{
	if (this->connection == nullptr)
	{
		this->connection = connection;
	}
}

} /* namespace db */
