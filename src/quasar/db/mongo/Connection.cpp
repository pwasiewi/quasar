#include "Connection.hpp"

namespace db_mongo
{

mongo::client::GlobalInstance instance;
bool initialized = false;

bool isInitialized()
{
	return db_mongo::initialized;
}

/**
 * Połączenie do bazy danych.
 * @param params parametry połączenia: host, port, nazwa bazy danych.
 */
Connection::Connection(const db::connectionParams& params) :
			db::Connection(params)
{
	init();
}

/**
 * Połączenie do bazy danych.
 * @param host adres hosta.
 * @param port port.
 * @param database nazwa używanej bazy danych.
 */
Connection::Connection(const std::string& host, const int port, const std::string& database) :
			db::Connection(host, port, database)
{
	init();
}

Connection::~Connection()
{

}

void Connection::init()
{
	try
	{
		if (!db_mongo::initialized)
		{
			if (!db_mongo::instance.initialized())
			{
				throw db::RuntimeError("Failed to initialize the client driver: " + instance.status().toString());
			}
			db_mongo::initialized = true;
		}
		if (!db_mongo::instance.status().isOK())
		{
			throw db::RuntimeError("Error with client driver: " + instance.status().toString());
		}
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("MongoDB::Connection construction failed."));
	}
}

db::DBType Connection::getDBType() const
{
	return db::DBType::MONGODB;
}

bool Connection::isConnected() const
{
	if (this->connection == nullptr)
	{
		return false;
	}
	return this->connection->isStillConnected();
}

/**
 * Rozpoczyna połączenie z bazą danych.
 */
void Connection::connect()
{
	const std::string hostport = host + ":" + std::to_string(port);
	try
	{
		this->connection = std::shared_ptr<mongo::DBClientConnection>(new mongo::DBClientConnection());
		this->connection->connect(hostport);

		// Catch złapię wyjątek mongo::DBException
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("Connection to " + hostport + " failed."));
	}
}

/**
 *
 * @param name
 * @param date
 * @param options
 * @param description
 * @return
 */
std::shared_ptr<db::ParameterizationResultSet> Connection::getNewParaResultSet(const std::string& name,
		const std::time_t date, db::ParameterizationResultSet::ParameterizationOptions options,
		const std::string& description) const
		{
	return std::shared_ptr<db::ParameterizationResultSet>(
			new ParameterizationResultSet(name, date, options, description));
}

/**
 *
 * @param prs
 */
void Connection::saveParaResultSet(std::shared_ptr<db::ParameterizationResultSet> prs)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki dzikie.
	// Jeżeli tak się dzieje to opakuje jest w DB::Exception (a tego wyjątku można się
	// spodziewać po tej klasie).
	try
	{
		// Sprawdzenie ustawień bazy danych, ewentualnie wprowadzenie odpowiednich ustawień
		setupDatabase();

		// Nie wiem czy jest potrzebne.
		//this->connection->resetError();
		std::shared_ptr<db_mongo::ParameterizationResultSet> mprs =
				std::dynamic_pointer_cast<db_mongo::ParameterizationResultSet>(prs);
		std::string ns = db_mongo::common::namespaceBuilder({ this->database, _para_result_set_col_name() });
		auto obj = mprs->getBSONObj();
		this->connection->insert(ns, obj);

		std::string e;
		if (!(e = this->connection->getLastError()).empty())
		{
			throw db::RuntimeError(e);
		}
		mprs->setOID(MONGO_COMMON_GET_OID(obj));
		mprs->setConnection(this->connection);
		mprs->setDatabase(this->database);
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("Connection::saveParaResultSet failed."));
	}
}

/**
 *
 * @param name
 * @param date
 * @param insertDate
 * @param size
 * @return
 */
std::shared_ptr<db::QuasarSet> Connection::getNewQuasarSet(const std::string& name, const std::time_t date,
		const std::time_t insertDate, const size_t size) const
		{
	return std::shared_ptr<db::QuasarSet>(new QuasarSet(name, date, insertDate, size));
}

/**
 * Zapisuje zestaw kwazarów w bazie danych.
 * @param qs zestaw kwazarów.
 */
void Connection::saveQuasarSet(std::shared_ptr<db::QuasarSet> qs)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki dzikie.
	// Jeżeli tak się dzieje to opakuje jest w DB::Exception (a tego wyjątku można się
	// spodziewać po tej klasie).
	try
	{
		// Sprawdzenie ustawień bazy danych, ewentualnie wprowadzenie odpowiednich ustawień
		setupDatabase();

		// Nie wiem czy jest potrzebne.
		//this->connection->resetError();
		std::shared_ptr<db_mongo::QuasarSet> mqs = std::dynamic_pointer_cast<db_mongo::QuasarSet>(qs);
		std::string ns = db_mongo::common::namespaceBuilder({ this->database, _quasarset_col_name() });
		auto obj = mqs->getBSONObj();
		this->connection->insert(ns, obj);

		std::string e;
		if (!(e = this->connection->getLastError()).empty())
		{
			throw db::RuntimeError(e);
		}
		mqs->setOID(MONGO_COMMON_GET_OID(obj));
		mqs->setConnection(this->connection);
		mqs->setDatabase(this->database);
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("Connection::saveQuasarSet failed."));
	}
}

/**
 * Usuwa zestaw kwazarów wraz ze wszystkimi jego kwazarami i lambdą.
 * @param qs zestaw kwazarów do usunięcia.
 */
void Connection::deleteQuasarSet(std::shared_ptr<db::QuasarSet> qs)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki dzikie.
	// Jeżeli tak się dzieje to opakuje jest w DB::Exception (a tego wyjątku można się
	// spodziewać po tej klasie).
	try
	{
		std::shared_ptr<db_mongo::QuasarSet> mqs = std::dynamic_pointer_cast<db_mongo::QuasarSet>(qs);

		// Usuwanie kwazarów z zestawu

		std::string ns = db_mongo::common::namespaceBuilder({ this->database, _quasar_col_name() });
		auto query = mongo::BSONObjBuilder().append("quasar_set_oid", mqs->getOID()).obj();
		this->connection->remove(ns, query);

		// Usuwanie zestawu

		ns = db_mongo::common::namespaceBuilder({ this->database, _quasarset_col_name() });
		// Zapytanie z _id QuasarSet
		query = mongo::BSONObjBuilder().append("_id", mqs->getOID()).obj();
		// Usuwanie dokładnie jednego.
		this->connection->remove(ns, query, true);

	} catch (const db_mongo::OIDInvalid& e)
	{
		std::throw_with_nested(
				db::RuntimeError("Connection::deleteQuasarSet failed: Cannot delete QuasarSet with invalid OID."));
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("Connection::deleteQuasarSet failed."));
	}
}

std::shared_ptr<db::QuasarSet> Connection::getQuasarSetByName(const std::string& name)
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki dzikie.
	// Jeżeli tak się dzieje to opakuje jest w DB::Exception (a tego wyjątku można się
	// spodziewać po tej klasie).
	std::shared_ptr<db::QuasarSet> qs;
	try
	{
		std::string ns = db_mongo::common::namespaceBuilder({ this->database, _quasarset_col_name() });
		auto query = mongo::BSONObjBuilder().append("name", name).obj();
		mongo::BSONObj bsonObj = this->connection->findOne(ns, query);

		if (!bsonObj.isEmpty())
		{
			qs = std::shared_ptr<db::QuasarSet>(new db_mongo::QuasarSet(bsonObj, this->database, this->connection));
		}
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("Connection::getQuasarSetByName failed."));
	}
	if (qs == nullptr)
	{
		std::string err = "Set \"" + name + "\" not found.";
		throw db::NotFound(err.c_str());
	}
	return qs;
}

/**
 * Pobiera wszystkie zestawy kwazarów z bazy danych.
 * Jeżeli spodziewalibyśmy się dużej ilości takich obiektów powinniśmy zamiast wektora
 * zwracać np. kursor. Jednak zakładam, że zestawów jest mało.
 * @return wskaźnik std::unique_ptr na wektor zestawów kwazarów.
 */
std::unique_ptr<db::Connection::QuasarSets> Connection::getQuasarSets()
{
	// Operacje mongo:: mogą rzucać jakieś wyjątki dzikie.
	// Jeżeli tak się dzieje to opakuje jest w DB::Exception (a tego wyjątku można się
	// spodziewać po tej klasie).
	std::unique_ptr<db::Connection::QuasarSets> vqs(new db::Connection::QuasarSets());
	try
	{
		std::string ns = db_mongo::common::namespaceBuilder({ this->database, _quasarset_col_name() });

		auto cursor = this->connection->query(ns, mongo::BSONObj());
		while (cursor->more())
		{
			mongo::BSONObj qs = cursor->next();
			vqs->push_back(
					std::shared_ptr<db::QuasarSet>(new db_mongo::QuasarSet(qs, this->database, this->connection)));
		}
	} catch (const db_mongo::OIDInvalid& e)
	{
		std::throw_with_nested(
				db::RuntimeError("Connection::deleteQuasarSet failed: Cannot delete QuasarSet with invalid OID."));
	} catch (const std::exception& e)
	{
		std::throw_with_nested(db::RuntimeError("Connection::deleteQuasarSet failed."));
	}
	return vqs;
}

/**
 * Zwraca bezpośrednie połączenie do bazy MongoDB.
 * @return obiekt połączenie do MongoDB.
 */
std::shared_ptr<mongo::DBClientConnection> Connection::getMongoConnection() const
{
	return this->connection;
}

/**
 * Ustawia bazę danych.
 */
void Connection::setupDatabase()
{
	std::string ns = db_mongo::common::namespaceBuilder({ this->database, _quasarset_col_name() });
	if (!this->connection->exists(ns))
	{
		this->connection->createCollection(ns);
		this->connection->createIndex(ns, mongo::IndexSpec().unique(true).addKey("name").name("qs_name_idx"));
	}
	ns = db_mongo::common::namespaceBuilder({ this->database, _quasar_col_name() });
	if (!this->connection->exists(ns))
	{
		this->connection->createCollection(ns);
		this->connection->createIndex(ns, mongo::IndexSpec().unique(true)
				.addKey("quasar_set_oid")
				.addKey("params.mjd").addKey("params.plate").addKey("params.fiber").name("quasar_setid_mjd_p_f_idx"));
		this->connection->createIndex(ns, mongo::IndexSpec().unique(false)
				.addKey("params.mjd").addKey("params.plate").addKey("params.fiber").name("quasar_mjd_p_f_idx"));
//		this->connection->createIndex(ns, mongo::IndexSpec().unique()
//				.addKey("params.name").name("quasar_name_idx"));
	}
	ns = db_mongo::common::namespaceBuilder({ this->database, _para_result_set_col_name() });
	if (!this->connection->exists(ns))
	{
		this->connection->createCollection(ns);
		this->connection->createIndex(ns, mongo::IndexSpec().unique(true).addKey("name").name("prs_name_idx"));
		this->connection->createIndex(ns, mongo::IndexSpec().unique(false).addKey("set_name").name("prs_set_name_idx"));
	}
	ns = db_mongo::common::namespaceBuilder({ this->database, _para_result_col_name() });
	if (!this->connection->exists(ns))
	{
		this->connection->createCollection(ns);
		this->connection->createIndex(ns, mongo::IndexSpec().unique(false)
				.addKey("mjd").addKey("plate").addKey("fiber").name("pr_mjd_p_f_idx"));
		this->connection->createIndex(ns, mongo::IndexSpec().unique(true)
				.addKey("para_result_set_oid")
				.addKey("mjd").addKey("plate").addKey("fiber").name("pr_psetid_mjd_p_f_idx"));
	}
}

} /* namespace db_mongo */

