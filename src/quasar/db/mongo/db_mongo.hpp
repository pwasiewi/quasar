#pragma once

// MONGO
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#endif
#include <mongo/client/dbclient.h>
// MONGO

#include "common.hpp"
#include "Exception.hpp"

namespace db_mongo
{

class Connection;
class QuasarSet;
class Exception;
class QuasarCursor;
class ParameterizationResultSet;

extern const std::string mongo_collection_names[4];

inline const std::string& _quasarset_col_name()
{
	return mongo_collection_names[0];
}

inline const std::string& _quasar_col_name()
{
	return mongo_collection_names[1];
}

inline const std::string& _para_result_set_col_name()
{
	return mongo_collection_names[2];
}

inline const std::string& _para_result_col_name()
{
	return mongo_collection_names[3];
}

}
