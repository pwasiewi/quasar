#pragma once

#include "db_mongo.hpp"
#include "bson_helper.hpp"
#include "QuasarCursor.hpp"
#include "common.hpp"

#include "../QuasarSet.hpp"
#include "../Exception.hpp"

#include <ctime>

namespace db_mongo
{

class QuasarSet: public db::QuasarSet
{
		friend class db_mongo::Connection;

	public:
		QuasarSet(const std::string&, const std::time_t, const std::time_t, const size_t size);
		QuasarSet(const std::string&, const std::time_t, const std::time_t, const size_t size,
				const mongo::OID& oid);
		QuasarSet(const mongo::BSONObj&);
		~QuasarSet();

		std::shared_ptr<db::QuasarCursor> getQuasarCursor();
		std::vector<Quasar> getQuasars(size_t);
		void addQuasars(std::vector<Quasar>);

		mongo::BSONObj getBSONObj() const;
		const mongo::OID& getOID() const;

	private:

		QuasarSet(const std::string&, const std::time_t, const std::time_t, const size_t size, const mongo::OID& oid,
				const std::string& database, const std::shared_ptr<mongo::DBClientConnection> connection);
		QuasarSet(const mongo::BSONObj&, const std::string& database,
				const std::shared_ptr<mongo::DBClientConnection> connection);

		void setOID(const mongo::OID oid);
		void setDatabase(const std::string& database);
		void setConnection(const std::shared_ptr<mongo::DBClientConnection> connection);

		void update();
		void deleteQuasars(std::vector<mongo::OID>& oids);

		/**
		 * Dostępne tylko podczas załadowania QuasarSet z bazy danych, lub po zapisaniu do niej.
		 */
		mongo::OID oid;

		/**
		 * Baza danych
		 */
		std::string database;

		/**
		 * Połączenie z MongoDB.
		 */
		std::shared_ptr<mongo::DBClientConnection> connection;
};

} /* namespace db_mongo */
