#pragma once

#include <initializer_list>
#include <string>
#include <ctime>
#include <chrono>

#include "../../Quasar/Quasar.hpp"

// MONGO
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#endif
#include <mongo/client/dbclient.h>
// MONGO

#ifndef MONGO_COMMON_GET_OID
#define MONGO_COMMON_GET_OID( obj ) \
	obj.getField("_id").OID()
#endif

namespace db_mongo
{

namespace common
{

std::string namespaceBuilder(std::initializer_list<std::string> names);
mongo::Date_t to_Date_t(const std::time_t tt);
std::time_t to_time_t(const mongo::Date_t dt);

} /* namespace common */

} /* namespace db_mongo */
