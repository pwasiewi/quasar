#include "LoadFilesToDBOptionsParser.hpp"

/**
 * Konstruktor, inicjalizuje parser.
 */
LoadFilesToDBOptionsParser::LoadFilesToDBOptionsParser() :
			general(boost::program_options::options_description("Load files to database task options"))
{
	namespace po = boost::program_options;

	this->hidden.add_options()
	("database_address",
			po::value<db::connectionParams>()->required(),
			"Full database address.")
	/* Opcja jest nieużywana, bo obecnie jest jedna implementacja połączenia do bazy danych */
	("database_type",
			po::value<std::string>()->default_value("mongodb"),
			"Database type (default: mongodb).")
			;

	this->general.add_options()
	("path,P",
			po::value<std::string>()->required(),
			"Path to quasars folder.")
	("quasarset_name,N",
			po::value<std::string>()->required(),
			"Quasar set name.")
	("quasarset_date,D",
			po::value<std::time_t>(),
			"Quasar set data (format: DD.MM.YYYY).")
			;

	this->all.add(general).add(hidden);
}

LoadFilesToDBOptionsParser::~LoadFilesToDBOptionsParser()
{

}

/**
 * Zwraca napis opisujący opcje zadania.
 * @return napis w czytelny sposób opisujący opcje zadania LoadFilesToDBTask.
 */
std::string LoadFilesToDBOptionsParser::getOptionsDescription() const
{
	std::ostringstream ss;
	ss << (this->general);
	return ss.str();
}

/**
 * Parsuje wektor napisów (opcje tekstowe) do obiektu opcji zadania,
 * w tym przypadku LoadFilesToDBOptions.
 * @param options wektor napisów (opcje tekstowe postaci: {"--opcja", "wartość", (...)}; patrz boost::program_options).
 * @return wskaźnik wskazujący na obiekt klasy LoadFilesToDBOptions.
 */
std::shared_ptr<TaskOptions> LoadFilesToDBOptionsParser::parse(const std::vector<std::string>& options)
{
	std::shared_ptr<LoadFilesToDBOptions> taskOptions(new LoadFilesToDBOptions());

	namespace po = boost::program_options;

	po::positional_options_description positional;
	positional.add("database_address", 1);
	positional.add("dummy", -1);

	po::variables_map vm;
	auto parsed_options = po::command_line_parser(options)
			.options(this->all)
			.positional(positional)
			.run();
	po::store(parsed_options, vm);

	// NOTIFY - dopiero tutaj sprawdzana program rzuca wyjątki
	// jeżeli coś jest nie tak z opcjami uruchomienia.
	po::notify(vm);

	taskOptions->setConnection(db::buildConnection(vm["database_address"].as<db::connectionParams>()));
	taskOptions->setQuasarSetName(vm["quasarset_name"].as<std::string>());

	auto path = vm["path"].as<std::string>();
	taskOptions->setFilesLoader(std::shared_ptr<QuasarFilesLoader>(new FitFilesLoader(path)));

	if (vm.count("quasarset_date"))
	{
		taskOptions->setQuasarSetDate(vm["quasarset_date"].as<std::time_t>());
	}

	return std::dynamic_pointer_cast<TaskOptions>(taskOptions);
}
