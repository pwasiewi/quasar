#pragma once

#include "../Task.hpp"
#include "LoadFilesToDBOptions.hpp"

#include "quasar/common/guard.hpp"

#include "quasar/Quasar/Quasar.hpp"
#include "quasar/QuasarFilesLoader/FitFilesLoader.hpp"

// DB
#include "quasar/db/db.hpp"
#include "quasar/db/Connection.hpp"
#include "quasar/db/QuasarSet.hpp"

#include <ctime>
#include <string>
#include <future>

/**
 * Klasa definiująca zadanie wczytanie kwazarów z pliku i zapisanie ich
 * jako zestaw kwazarów w bazie danych.
 */
class LoadFilesToDBTask: public Task
{
	public:

		LoadFilesToDBTask(const LoadFilesToDBOptions);
		LoadFilesToDBTask(std::shared_ptr<TaskOptions>);
		~LoadFilesToDBTask();
		void execute();

	private:
		std::shared_ptr<LoadFilesToDBOptions> options;

		std::shared_ptr<QuasarFilesLoader> setupQuasarFilesLoader();
		std::shared_ptr<db::Connection> setupConnection();
		void checkOptions();
};

