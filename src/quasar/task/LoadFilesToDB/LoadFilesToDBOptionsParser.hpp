#pragma once

#include "../TaskOptionsParser.hpp"
#include "LoadFilesToDBOptions.hpp"

#include "quasar/QuasarFilesLoader/FitFilesLoader.hpp"

// DB
#include "quasar/db/db.hpp"
#include "quasar/db/Connection.hpp"

#include "quasar/validators.hpp"

#include <boost/program_options.hpp>

#include <sstream>

/**
 * Klasa odpowiedzialna za parsowanie opcji tekstowych (patrz boost::program_options)
 * do obiektu opcji zadania.
 */
class LoadFilesToDBOptionsParser: public TaskOptionsParser
{
	public:
		LoadFilesToDBOptionsParser();
		~LoadFilesToDBOptionsParser();
		std::shared_ptr<TaskOptions> parse(const std::vector<std::string>& options);
		std::string getOptionsDescription() const;

	private:
		boost::program_options::options_description all;
		boost::program_options::options_description general;
		boost::program_options::options_description hidden;
};
