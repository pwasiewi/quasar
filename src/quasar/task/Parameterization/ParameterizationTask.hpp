#pragma once

#include "../Task.hpp"
#include "ParameterizationOptions.hpp"

#include "quasar/Quasar/Quasar.hpp"

// OpenCL
#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include "quasar/algorithm/SpectralLine.hpp"
#include "quasar/algorithm/FeTemplate.hpp"

#include "quasar/algorithm/specparameterization/SpecParameterization.hpp"
#include "quasar/algorithm/fefitwin/FeFitWin.hpp"

// DB
#include "quasar/db/db.hpp"
#include "quasar/db/Connection.hpp"
#include "quasar/db/QuasarSet.hpp"

#include <ctime>
#include <string>
#include <future>

/**
 *
 */
class ParameterizationTask: public Task
{
	public:

		ParameterizationTask(const ParameterizationOptions);
		ParameterizationTask(std::shared_ptr<TaskOptions>);
		~ParameterizationTask();
		void execute();

	private:
		std::shared_ptr<ParameterizationOptions> options;

		void setupConnections();
		void checkOptions();
};

