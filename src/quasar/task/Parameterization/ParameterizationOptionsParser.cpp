#include "ParameterizationOptionsParser.hpp"

/**
 * Konstruktor, inicjalizuje parser.
 */
ParameterizationOptionsParser::ParameterizationOptionsParser() :
			general(boost::program_options::options_description("Parameterization task options", 100))
{
	namespace po = boost::program_options;

	this->hidden.add_options()
	("database_address",
			po::value<db::connectionParams>()->required(),
			"Database address.")
	/* Opcja jest nieużywana, bo obecnie jest jedna implementacja połączenia do bazy danych */
	("database_type",
			po::value<std::string>()->default_value("mongodb"),
			"Database type: mongodb.")
			;

	float amp_wl_default = 3000.0f;
	float fe_fwhmn_default = 1600.0f;
	float fe_fwhmt_default = 900.0f;
	float fe_scale_rate_default = 1.0f;
	cl_float2 fe_fit_range_default = { { 2200.0f, 2650.0f } };
	std::string fe_fit_range_default_text = "[2200.0; 2650.0]";

	auto fe_fit_type_default = algorithm::ocl::FeFitWin::Type::WIN;
	std::string fe_fit_type_default_text = "WIN";

	this->general.add_options()
	("set_name,N",
			po::value<std::string>()->required(),
			"Astronomical objects set name (in database).")
	("spectral_lines",
			po::value<std::string>(),
			"Path to a file containing spectral lines.")
	("cont_wins",
			po::value<std::string>(),
			"Path to a file containing continuum spectral windows (wavelength intervals [A]).")
	("amp_wl",
			po::value<float>()->default_value(amp_wl_default),
			"Continuum power-law function amplitude wavelength [A].")
	("fe_wins",
			po::value<std::string>(),
			"Path to a file containing iron emission template spectral windows (wavelength intervals [A]).")
	("fe_temp",
			po::value<std::string>()->required(),
			"Path to a file containing iron emission template.")
	("fe_fwhmn",
			po::value<float>()->default_value(fe_fwhmn_default),
			"Full-Width at Half-Maximum of objects' spectral lines.")
	("fe_fwhmt",
			po::value<float>()->default_value(fe_fwhmt_default),
			"Full-Width at Half-Maximum of iron emission template spectral.")
	("fe_scale_rate",
			po::value<float>()->default_value(fe_scale_rate_default),
			"Additional scale rate of iron emission.")
	("fe_fit_range",
			po::value<cl_float2>()->default_value(fe_fit_range_default, fe_fit_range_default_text),
			"Main iron emission wavelength interval [A].")
	("fe_fit_type",
			po::value<algorithm::ocl::FeFitWin::Type>()->default_value(fe_fit_type_default, fe_fit_type_default_text),
			"Iron template fitting type: FULL, FWIN, WIN.")
			;
	this->all.add(general).add(hidden);
}

ParameterizationOptionsParser::~ParameterizationOptionsParser()
{

}

/**
 * Zwraca napis opisujący opcje zadania.
 * @return napis w czytelny sposób opisujący opcje zadania ParameterizationTask.
 */
std::string ParameterizationOptionsParser::getOptionsDescription() const
{
	std::ostringstream ss;
	ss << (this->general);
	return ss.str();
}

/**
 * Parsuje wektor napisów (opcje tekstowe) do obiektu opcji zadania,
 * w tym przypadku ParameterizationOptions.
 * @param options wektor napisów (opcje tekstowe postaci: {"--opcja", "wartość", (...)}; patrz boost::program_options).
 * @return wskaźnik wskazujący na obiekt klasy ParameterizationOptions.
 */
std::shared_ptr<TaskOptions> ParameterizationOptionsParser::parse(const std::vector<std::string>& options)
{
	std::shared_ptr<ParameterizationOptions> taskOptions(new ParameterizationOptions());

	namespace po = boost::program_options;

	po::positional_options_description positional;
	positional.add("database_address", 1);
	positional.add("dummy", -1);

	po::variables_map vm;
	auto parsed_options = po::command_line_parser(options)
			.options(this->all)
			.positional(positional)
			.run();
	po::store(parsed_options, vm);

	// NOTIFY - dopiero tutaj sprawdzana program rzuca wyjątki
	// jeżeli coś jest nie tak z opcjami uruchomienia.
	po::notify(vm);

	taskOptions->setReadConnection(db::buildConnection(vm["database_address"].as<db::connectionParams>()));
	taskOptions->setWriteConnection(db::buildConnection(vm["database_address"].as<db::connectionParams>()));
	taskOptions->setAstroObjSetName(vm["set_name"].as<std::string>());
	taskOptions->setAstroObjN(vm["n_objs"].as<size_t>());
	taskOptions->setAmpWavelength(static_cast<cl_float>(vm["amp_wl"].as<float>()));

	// Ustawienie wbudowanych
	taskOptions->setSpectralLines(data::spectralLines);
	taskOptions->setContinuumWindows(data::contwinfull);
	taskOptions->setFeWindows(data::fewinfull);

	taskOptions->setSpectralLinesFile("built-in");
	taskOptions->setContinuumWindowsFile("built-in");
	taskOptions->setFeWindowsFile("built-in");

	ParameterizationOptions::FeFitParameters fitParameteres =
			{
					static_cast<cl_float>(vm["fe_fwhmn"].as<float>()),
					static_cast<cl_float>(vm["fe_fwhmt"].as<float>()),
					static_cast<cl_float>(vm["fe_scale_rate"].as<float>()),
					vm["fe_fit_range"].as<cl_float2>(),
					false,
					vm["fe_fit_type"].as<algorithm::ocl::FeFitWin::Type>()
			};
	taskOptions->setFeFitParameters(fitParameteres);

	// Wczytanie szablonu żelaza
	taskOptions->setFeTemplate(common::file::loadFeTemplate(vm["fe_temp"].as<std::string>()));
	taskOptions->setFeTemplateFile(vm["fe_temp"].as<std::string>());

	// Wczytanie elementów
	if (vm.count("spectral_lines"))
	{
		taskOptions->setSpectralLines(common::file::loadSpectralLines(vm["spectral_lines"].as<std::string>()));
		taskOptions->setSpectralLinesFile(vm["spectral_lines"].as<std::string>());
	}

	// Wczytanie okien
	if (vm.count("cont_wins"))
	{
		taskOptions->setContinuumWindows(common::file::loadWindows(vm["cont_wins"].as<std::string>()));
		taskOptions->setContinuumWindowsFile(vm["cont_wins"].as<std::string>());
	}

	// Wczytanie okien
	if (vm.count("fe_wins"))
	{
		taskOptions->setFeWindows(common::file::loadWindows(vm["fe_wins"].as<std::string>()));
		taskOptions->setFeWindowsFile(vm["fe_wins"].as<std::string>());
	}

	return std::dynamic_pointer_cast<TaskOptions>(taskOptions);
}
