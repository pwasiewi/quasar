#pragma once

#include <string>
#include <stdexcept>
#include <algorithm>

enum class TaskType : int
{
		LoadFilesToDB,
		Parameterization
};

TaskType to_taskType(const std::string&);
std::string to_string(TaskType taskType);
