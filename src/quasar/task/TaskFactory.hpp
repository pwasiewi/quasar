#pragma once

#include "Task.hpp"
#include "TaskType.hpp"
#include "TaskOptions.hpp"

#include "LoadFilesToDB/LoadFilesToDBTask.hpp"
#include "Parameterization/ParameterizationTask.hpp"

#include <memory>
#include <string>

class TaskFactory
{
	public:
		TaskFactory();
		virtual ~TaskFactory();
		virtual std::unique_ptr<Task> create(TaskType, std::shared_ptr<TaskOptions>);
};
