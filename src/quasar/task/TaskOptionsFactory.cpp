#include "TaskOptionsFactory.hpp"

TaskOptionsFactory::TaskOptionsFactory()
{

}

TaskOptionsFactory::~TaskOptionsFactory()
{

}

std::shared_ptr<TaskOptions> TaskOptionsFactory::create(TaskType taskType)
{
	std::shared_ptr<TaskOptions> taskOptions = nullptr;

	switch (taskType)
	{
		case TaskType::LoadFilesToDB:
			taskOptions.reset(new LoadFilesToDBOptions());
			break;
		case TaskType::Parameterization:
			taskOptions.reset(new ParameterizationOptions());
			break;
		default:
			throw std::logic_error("Unknown task type.");
	}
	return taskOptions;
}

std::shared_ptr<TaskOptions> TaskOptionsFactory::create(TaskType taskType, const std::vector<std::string>& options)
{
	std::shared_ptr<TaskOptions> taskOptions = nullptr;

	switch (taskType)
	{
		case TaskType::LoadFilesToDB:
			taskOptions = LoadFilesToDBOptionsParser().parse(options);
			break;
		case TaskType::Parameterization:
			taskOptions = ParameterizationOptionsParser().parse(options);
			break;
		default:
			throw std::logic_error("Unknown task type.");
	}
	return taskOptions;
}

std::shared_ptr<TaskOptionsParser> TaskOptionsFactory::getParser(TaskType taskType)
{
	std::shared_ptr<TaskOptionsParser> taskOptionsParser = nullptr;

	switch (taskType)
	{
		case TaskType::LoadFilesToDB:
			taskOptionsParser.reset(new LoadFilesToDBOptionsParser());
			break;
		case TaskType::Parameterization:
			taskOptionsParser.reset(new ParameterizationOptionsParser());
			break;
		default:
			throw std::logic_error("Unknown task type.");
	}
	return taskOptionsParser;
}
