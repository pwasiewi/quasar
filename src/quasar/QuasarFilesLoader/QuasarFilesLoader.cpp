#include "QuasarFilesLoader.hpp"

/**
 * Konstruktor.
 * @param path ścieżka do folderu, w którym są kwazary do wczytania.
 */
QuasarFilesLoader::QuasarFilesLoader(std::string path)
:
			initialized(false),
			dirPath(path)
{
	if (this->dirPath.empty())
	{
		throw std::logic_error("Directory path cannot be an empty string.");
	}
}

QuasarFilesLoader::~QuasarFilesLoader(void)
{

}

bool QuasarFilesLoader::isInitialized()
{
	return this->initialized;
}

const std::string& QuasarFilesLoader::getPath()
{
	return this->dirPath;
}
