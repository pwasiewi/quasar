#include "FitFilesLoader.hpp"

/**
 * Konstruktor.
 * @param path ścieżka do folderu, w którym są kwazary do wczytania.
 * @param quasarDataExt rozszerzenie pliku zawierającego dane o widmie kwazaru (wartości y punktów (x,y) opisujących widmo).
 * @param quasarParamExt rozszerzenie pliku z parametrami kwazaru.
 */
FitFilesLoader::FitFilesLoader(std::string path)
:
			QuasarFilesLoader(path),
			quasarFileExt(boost::filesystem::path(".fit"))

{
	if (this->dirPath.empty())
	{
		throw std::logic_error("Directory path cannot be an empty string.");
	}
}

FitFilesLoader::~FitFilesLoader(void)
{

}

void FitFilesLoader::init()
{
	if (!this->initialized)
	{
		findQuasarFiles();
		if (this->quasarFiles.empty())
		{
			throw std::logic_error("Quasars not found in: " + this->dirPath);
		}
		this->quasarFilesIterator = this->quasarFiles.cbegin();
		this->initialized = true;
		return;
	}
	throw std::runtime_error("FitFilesLoader already initialized.");
}

void FitFilesLoader::reset()
{
	if (!this->initialized)
	{
		throw std::runtime_error("FitFilesLoader is not initialized");
	}
	this->quasarFilesIterator = this->quasarFiles.cbegin();
}

/**
 * Przeszukuje folder dirPath w poszukiwaniu kwazarów.
 */
void FitFilesLoader::findQuasarFiles()
{
	namespace fs = boost::filesystem;

	fs::path full_path(fs::initial_path<fs::path>());
	full_path = fs::system_complete(fs::path(this->dirPath));

	if (!fs::exists(full_path))
	{
		throw std::logic_error(full_path.string() + " path does not exist.");
	}

	if (!fs::is_directory(full_path))
	{
		throw std::logic_error(full_path.string() + " is not a directory.");
	}

	fs::directory_iterator end_iter;
	for (fs::directory_iterator dir_itr(full_path); dir_itr != end_iter; ++dir_itr)
	{
		fs::path temp_path;
		try
		{
			if (fs::is_regular_file(dir_itr->status()))
			{
				temp_path = dir_itr->path();
				if (temp_path.extension() == quasarFileExt)
				{
					this->quasarFiles.push_back(temp_path);
				}
			}
		} catch (const std::exception & ex)
		{
			//
			// WARNINGS
			// 
			std::cerr << "Warning: " << ex.what() << std::endl;
		}
	}
}

/**
 * Wczytuje n kolejnych kwazarów (z podanego w konstruktorze folderu).
 * Dla n = 0 wczytywane jest wszystko (domyślne działanie).
 * @param n liczba kwazarów do wczytania.
 * @return Wektor kwazarów.
 */
std::vector<Quasar> FitFilesLoader::loadn(unsigned long n)
{
	if (!this->initialized)
	{
		throw std::runtime_error("FitFilesLoader is not initialized");
	}

	std::vector<Quasar> quasars;

	boost::filesystem::path filePath;
	unsigned long i = 0;
	bool loadAll = n == 0;

	while ((i < n || loadAll) && this->quasarFilesIterator != this->quasarFiles.cend())
	{
		filePath = *(this->quasarFilesIterator);

		try
		{
			quasars.push_back(QuasarFactory::createQuasarFromFit(filePath));
		} catch (const std::exception& ex)
		{
			//
			// WARNING
			//
			std::cerr << "Warning:" << std::endl;
			std::cerr << "\tException has occurred while creating Quasar from files:\n";
			std::cerr << "\t\tFile: " << filePath.string() << "\n";
			std::cerr << "\tException: " << ex.what() << std::endl;
			std::cerr << "\tFiles have been ignored." << ex.what() << std::endl;
		}

		this->quasarFilesIterator++;
		i++;
	}
	return quasars;
}

std::size_t FitFilesLoader::getSize()
{
	if (!this->initialized)
	{
		throw std::runtime_error("FitFilesLoader is not initialized");
	}
	return this->quasarFiles.size();
}
