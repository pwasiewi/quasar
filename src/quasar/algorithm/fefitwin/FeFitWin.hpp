#pragma once

#include "../defines.hpp"

#include "../utils.hpp"
#include "../spectools/SpecTools.hpp"
#include "../tools/Tools.hpp"
#include "../basicmatrix/BasicMatrix.hpp"
#include "../FeTemplate.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <algorithm>

namespace algorithm
{

namespace ocl
{

class FeFitWin
{
	public:

		/**
		 * Struktura dla zwracania wyników.
		 */
		struct Results
		{
				cl::Buffer feTemplateMatrix;
				std::vector<cl_float> scaleRates;
				std::vector<cl_uint> sizes_fewindows;
				std::vector<cl_float> reducedChisqs_fewindows;
				std::vector<cl_float> reducedChisqs_full;
				std::vector<cl_float> ews_full;
				std::vector<cl_float> reducedChisqs_feRange;
				std::vector<cl_float> ews_feRange;
		};

		/**
		 * Rodzaj fitowanego obszaru
		 */
		enum class Type : int
		{
			FULL = 1,//!< FULL
			WIN = 2, //!< WIN
			FWIN = 3 //!< FWIN
		};

		/**
		 * Parametry fitowania.
		 */
		struct FitParameters
		{
				/**
				 * Narrow component Full-Width at Half-Maximum
				 */
				cl_float fwhmn;
				/**
				 * Time component Full-Width at Half-Maximum
				 */
				cl_float fwhmt;
				/**
				 * Współczynnik skalowania szablonu żelaza
				 */
				cl_float feScaleRate;
				/**
				 * Granice obszaru, w którym fitowane jest żelazo.
				 */
				cl_float2 feFitRange;
				/**
				 * Prawda, jeżeli od przekazanych widm odjęto odpowiadające im kontinua; wpp - fałsz.
				 */
				bool isSubC;
				/**
				 * Typ fitowanego obszaru.
				 */
				Type fitType;
		};

		/**
		 * Dane o widmach.
		 *
		 * Dokładnie zestaw macierzy, gdzie każda macierz w swojej
		 * KOLUMNIE ma dane dotyczące jednego widma. Dodatkowo wektor
		 * długości kolejnych widm.
		 */
		struct Data
		{
				/**
				 * Widma obiektów.
				 */
				const std::vector<cl_float>& spectrumsMatrixHost;
				/**
				 * Długości fal.
				 */
				const std::vector<cl_float>& wavelengthsMatrixHost;
				/**
				 * Błędy pomiaru widma.
				 */
				const std::vector<cl_float>& errorsMatrixHost;
				/**
				 * Kontinua.
				 */
				const std::vector<cl_float>& continuumsMatrixHost;
				/**
				 * Rozmiary widm.
				 */
				const std::vector<cl_uint>& sizes;
		};

		/**
		 * Bufory cl::Buffer z danymi o widmach.
		 *
		 * Dokładnie zestaw macierzy, gdzie każda macierz w swojej
		 * KOLUMNIE ma dane dotyczące jednego widma. Dodatkowo wektor
		 * długości kolejnych widm.
		 */
		struct Buffers
		{
				cl::Buffer& spectrumsMatrix;
				cl::Buffer& wavelengthsMatrix;
				cl::Buffer& errorsMatrix;
				cl::Buffer& continuumsMatrix;
				cl::Buffer& sizes;
		};

		FeFitWin(const cl::Context&, const cl::CommandQueue&);
		virtual ~FeFitWin();

		FeFitWin::Results run(const Data spectrumsData, const Buffers spectrumsBuffers, const size_t size,
				FeTemplate feTemplate,
				std::vector<cl_float2> feWindows,
				FitParameters fitParameteres = { 1600.0f, 900.0f, 1.0f, { { 2200.0f, 2650.0f } }, false, Type::WIN },
				const std::vector<cl::Event>* events = nullptr);

	private:

		cl::Buffer calcFeTemplateMatrix(cl::Buffer wavelengthsMatrix, cl::Buffer sizes, size_t size,
				FeTemplate feTemplate, FitParameters fitParameters);
		cl::Buffer calcFeTemplateMatrixScaleRates(cl::Buffer spectrumsMatrix, cl::Buffer templateFeMatrix,
				cl::Buffer sizes, size_t size, size_t max_spectrum_size,
				FitParameters fitParameters);
		cl::Buffer calcReducedChisqs(cl::Buffer& fs, cl::Buffer& ys, cl::Buffer errors,
				cl::Buffer sizes, size_t size, size_t max_spectrum_size);

		std::vector<cl_float> convolve(std::vector<cl_float> signal, std::vector<cl_float> kernel, bool same = true);
		void reduceChisqs(cl::Buffer& chisqs,
				cl::Buffer& sizes_filtered, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		static std::vector<std::string> ssources;

		/** Narzędzia */

		Tools tools;
		SpecTools specTools;
		BasicMatrix basicMatrix;

		/** Kernele, czyli różne operacje */

		cl::Kernel reduceChisqsKernel;
		static const char* reduceChisqsKernelName;

		/** End OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
