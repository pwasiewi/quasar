#pragma once

#include <CL/cl.h>

#include <string>
#include <stdexcept>

struct FeTemplate
{
		std::vector<cl_float> values;
		std::vector<cl_float> wavelengths;
};
