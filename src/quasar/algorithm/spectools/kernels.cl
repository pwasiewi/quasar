
#define ASTRO_OBJ_SPEC_SIZE 4096

__kernel void generateWavelengthsMatrix
	(
		__global float4 * abz_buffer,
		__global float * wavelengths_matrix
	)
{
	// gid0 - numer widma kwazaru
	uint gid0 = get_global_id(0);	
	
	// parametry a, b i z kwazaru
	float4 abz_;
	__local float4 local_abz_;

	if (get_local_id(0) == 0)
	{
  		local_abz_ = abz_buffer[gid0];
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	abz_ = local_abz_;
	
	// gid1 - numer elementu widma (indeks od 0 do 4095)
	uint gid1 = get_global_id(1);
	uint idx = gid0 * get_global_size(1) + gid1;
	
	// Wyliczenie lambdy dla tego gid1
	float wl = (abz_.x * (float)(gid1)) + abz_.y;
	wl = pow((float)(10),wl);
	
	// Uwzglednienie przesuniecia kosmologicznego - przejscie do ukladu emitujacego z wykorzystaniem redshiftu
	wavelengths_matrix[idx] = wl / (abz_.z + (float)(1));
	
	return;
}

// Dodaje do każdego z widm w spectrums_matrix jedno wskazane widmo
// z pojedynczą interpolacją.
//
// Widma, długości widm są tu ustawione kolumnami, a nie wierszami.
// dzięki temu otrzymujemy duży skok wydajności.
//
__kernel void addSpectrum
	(
		__global float * wavelengths_matrix,	// Długości dla widm
		__global float * spectrums_matrix,	// Widma po kolumnach
		__global uint  * sizes,			// Rozmiary widm
		uint size,				// Ilość widm (ilość kolumn)
		__constant float * to_add_wavelengths,	// Długości fal widma, które dodajemy
		__constant float * to_add_spectrum,	// Widmo, które dodajemy
		uint to_add_spectrum_size,	
		__global float * output_matrix		// Macierz zapisu wyniku sumy
	)
{
	// gid0 - numer widma kwazaru
	uint gid0 = get_global_id(0);
	if(gid0 >= size)
	{
		return;
	}	
	
	uint spectrum_size = sizes[gid0];

	// Indek elementu ze wavelengths_matrix/spectrums_matrix
	uint idx = gid0;
	uint idx_max = idx + spectrum_size * size;	
	
	// Indeks elementu z to_add_wavelengths/to_add_spectrum
	uint to_add_idx = 0;
	uint to_add_idx_max = to_add_spectrum_size - 1;
	
	float wavelength = wavelengths_matrix[idx];
	float value = spectrums_matrix[idx];
	
	float to_add_wl 	= to_add_wavelengths[to_add_idx];
	float to_add_wl_next 	= to_add_wavelengths[to_add_idx+1];		
	float to_add_value 	= to_add_spectrum[to_add_idx];
	float to_add_value_next = to_add_spectrum[to_add_idx+1];	
	

	uchar idx_max_flag = 0;
	uchar to_add_idx_max_flag = 0;

	while(1)
	{				
		if(wavelength >= to_add_wl)
		{			
			if(wavelength <= to_add_wl_next)
			{	
				float a = (to_add_value_next - to_add_value)/(to_add_wl_next - to_add_wl);
				float b = to_add_value - (a * to_add_wl);
				value = value + (a * wavelength + b);		
				output_matrix[idx] = value;		
				
				idx += size;
				// Sprawdzanie czy idx nie przekroczył idx_max
				// Zanim odczytamy dane.
				idx_max_flag = select(1, 0, idx < idx_max);
				if(idx_max_flag)
				{
					break;
				}
				wavelength = wavelengths_matrix[idx];
				value = spectrums_matrix[idx];
			}
			else
			{
				to_add_idx++;
				// Sprawdzanie czy to_add_idx nie przekroczył to_add_idx_max
				// Zanim odczytamy dane.
				to_add_idx_max_flag = select(1, 0, to_add_idx < to_add_idx_max);
				if(to_add_idx_max_flag)
				{
					break;
				}
				to_add_wl = to_add_wl_next;				
				to_add_wl_next = to_add_wavelengths[to_add_idx+1];
				
				to_add_value = to_add_value_next;
				to_add_value_next = to_add_spectrum[to_add_idx+1];
			}
		}
		else
		{	
			output_matrix[idx] = value;
		
			idx += size;
			// Sprawdzanie czy idx nie przekroczył idx_max
			// Zanim odczytamy dane.
			idx_max_flag = select(1, 0, idx < idx_max);
			if(idx_max_flag)
			{
				break;
			}
			wavelength = wavelengths_matrix[idx];
			value = spectrums_matrix[idx];
		}	
	}
	
	while(idx < idx_max)
	{
		value = spectrums_matrix[idx];
		output_matrix[idx] = value; 
		idx += size;
	}	
}


// Filtruje tylko te dane, których odpowiadająca wartość
// długości fali z wavelengths_matrix mieści się, w jakimś oknie widmowym.
//
__kernel void filterWithWavelengthWindows
	(
		__global float * wavelengths_matrix,	// Długości fal widm
		__global float * spectrums_matrix,	// Widma
		__global float * errors_matrix,		// Błędy pomiaru widm
		__global uint  * sizes,		 	// Rozmiary widm w spectrums_matrix
		__constant float2 * windows,	// Okna	długości fal
		uint windows_size 		// Ilość okien
	)	
{
	// gid0 - numer widma kwazaru
	uint gid0 = get_global_id(0);
	// gid1 - numer elementu w widmie
	uint gid1 = get_global_id(1);
	// indeks 
	uint idx = (gid0 * ASTRO_OBJ_SPEC_SIZE) + gid1;

	uint size = sizes[idx % get_global_size(0)];

	// Zero wskazuje na brak dopasownia do któregokolwiek okna
	int global_flag = 0;

	float wl_result = wavelengths_matrix[idx];
	float spec_result = spectrums_matrix[idx];
	float error_result = errors_matrix[idx];

	uint window_idx = 0;	
	float2 window;
	__local float2 window_local;
	
	// Pętla po wszystkich oknach
	int window_flag;
	for(; window_idx < windows_size; window_idx++)
	{
		window_flag = 0;
		if (get_local_id(0) == 0)
		{
  			window_local = windows[window_idx];		
		}
		barrier(CLK_LOCAL_MEM_FENCE);

		window = window_local;

		window_flag = select(0, 1, wl_result >= window.x);
		window_flag *= select(0, 1, wl_result <= window.y);
		// Jeżeli oba warunki spełnione to mamy dopasowania
		// W przeciwnym przypadku zostawiamy to co było.
		global_flag = select(global_flag, 1, window_flag == 1);
	}

	spec_result = select(INFINITY, spec_result, global_flag == 1);
	wl_result = select(INFINITY, wl_result, global_flag == 1);
	error_result = select(INFINITY, error_result, global_flag == 1);	

	uint row_idx = idx / get_global_size(0);
	spec_result = select(INFINITY, spec_result, row_idx < size);
	wl_result = select(INFINITY, wl_result, row_idx < size);
	error_result = select(INFINITY, error_result, row_idx < size);

	wavelengths_matrix[idx] = wl_result;	
	spectrums_matrix[idx] = spec_result;
	errors_matrix[idx] = error_result;
}


// Filtruje tylko te dane, których odpowiadające
// wartości ze spectrum_matrix są większe od zera.
//
__kernel void filterNonpositive
	(		
		__global float * spectrums_matrix,	// Widma
		__global float * a_matrix,	// Dowolna macierz wielkości spectrums_matrix
		__global float * b_matrix,	// Dowolna macierz wielkości spectrums_matrix
		__constant uint * sizes 	// Rozmiary widm w spectrums_matrix
	)
{
	uint gid0 = get_global_id(0);
	uint gid1 = get_global_id(1);
	// Indeks elementu
	uint idx = (gid0 * ASTRO_OBJ_SPEC_SIZE) + gid1;

	uint size = sizes[idx % get_global_size(0)];
	uint row_idx = idx / get_global_size(0);

	float spectrum = spectrums_matrix[idx];

	// Obliczenie flagi, czy spec_result > 0.
	// Zero wskazuje na liczbe mniejszą równą zero
	int flag = select(0, 1, spectrum >= FLT_MIN);
	// Dodatkowe sprawdzenie, czy element jest znaczący.
	flag *= select(0, 1, row_idx < size);

	spectrum = select(INFINITY, spectrum, flag);
	spectrums_matrix[idx] = spectrum;

	float a = a_matrix[idx];
	a = select(INFINITY, a, flag);
	a_matrix[idx] = a;	

	float b = b_matrix[idx];
	b = select(INFINITY, b, flag);	
	b_matrix[idx] = b;
}


// Filtruje tylko te dane, których odpowiadające
// wartości ze spectrums_matrix, są różne od zera.
//
__kernel void filterZeros
	(
		__global float * spectrums_matrix,	// Widma
		__global float * a_matrix,	// Dowolna macierz wielkości spectrums_matrix
		__global float * b_matrix,	// Dowolna macierz wielkości spectrums_matrix
		__constant uint * sizes 	// Rozmiary widm w spectrums_matrix
	)
{
	uint gid0 = get_global_id(0);
	uint gid1 = get_global_id(1);
	// Indeks elementu
	uint idx = (gid0 * ASTRO_OBJ_SPEC_SIZE) + gid1;

	uint size = sizes[idx % get_global_size(0)];
	uint row_idx = idx / get_global_size(0);

	float spectrum = spectrums_matrix[idx];

	// Obliczenie flagi, czy spec_result != 0.
	int flag = select(0, 1, spectrum != 0.0f);
	// Dodatkowe sprawdzenie, czy element jest znaczący.
	flag *= select(0, 1, row_idx < size);

	spectrum = select(INFINITY, spectrum, flag);
	spectrums_matrix[idx] = spectrum;

	float a = a_matrix[idx];
	a = select(INFINITY, a, flag);
	a_matrix[idx] = a;	

	float b = b_matrix[idx];
	b = select(INFINITY, b, flag);	
	b_matrix[idx] = b;
}

// Filtruje tylko te dane, których odpowiadające
// wartości ze spectrums_matrix są różne od +/- INFINITY.
//
__kernel void filterInfs
	(
		__global float * spectrums_matrix,	// Widma
		__global float * a_matrix,	// Dowolna macierz wielkości spectrums_matrix
		__global float * b_matrix,	// Dowolna macierz wielkości spectrums_matrix
		__constant uint * sizes 	// Rozmiary widm w spectrums_matrix
	)
{
	uint gid0 = get_global_id(0);
	uint gid1 = get_global_id(1);
	// Indeks elementu
	uint idx = (gid0 * ASTRO_OBJ_SPEC_SIZE) + gid1;

	uint size = sizes[idx % get_global_size(0)];
	uint row_idx = idx / get_global_size(0);

	float spectrum = spectrums_matrix[idx];

	// Obliczenie flagi, czy spec_result != +/- INFINITY.
	int flag = select(0, 1, spectrum != INFINITY);
	flag *= select(0, 1, spectrum != -INFINITY);
	// Dodatkowe sprawdzenie, czy element jest znaczący.
	flag *= select(0, 1, row_idx < size);

	spectrum = select(INFINITY, spectrum, flag);
	spectrums_matrix[idx] = spectrum;

	float a = a_matrix[idx];
	a = select(INFINITY, a, flag);
	a_matrix[idx] = a;
		
	if(b_matrix != 0)
	{
		float b = b_matrix[idx];
		b = select(INFINITY, b, flag);	
		b_matrix[idx] = b;
	}
}
