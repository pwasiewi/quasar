#pragma once

#include "../defines.hpp"

#include "../utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>

namespace algorithm
{

namespace ocl
{

/**
 * Klasa udostępniająca metody przetwarzające macierze widm (widm, długości fal, błędów).
 *
 * UWAGI:
 *
 * - Metody tej klasy zakładają, że maksymalna długość widma wynosi 4096 elementów (definicja ASTRO_OBJ_SPEC_SIZE),
 * 	 a podawane bufory cl::Buffer są rozmiaru N x 4096 (N - ilość widm).
 * - Większość metod działa na macierzach, w których widma są ułożone kolumnowo (N kolumn, 4096 wierszy). Wyjątkiem
 *   jest generateWavelengthsMatrix, która generuje macierz długości fal dla widm "wierszowo" (N wierszy, 4096 kolumn).
 *   Należy więc zwykle macierzy wynikową transponować.
 *
 */
class SpecTools
{
	public:
		SpecTools(const cl::Context&, const cl::CommandQueue&);
		virtual ~SpecTools();

		void generateWavelengthsMatrix(cl::Buffer& abz_, const size_t size, cl::Buffer& wavelengthsMatrix,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void addSpectrum(cl::Buffer& spectrumsMatrix, cl::Buffer& wavelengthsMatrix,
				cl::Buffer& sizes, const size_t size,
				cl::Buffer& toAddWavelengths, cl::Buffer& toAddSpectrum, const size_t toAddSize,
				cl::Buffer& outputSpectrumsMatrix,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void filterWithWavelengthWindows(cl::Buffer& spectrumsMatrix, cl::Buffer& wavelengthsMatrix,
				cl::Buffer& errorsMatrix,
				cl::Buffer& sizes, const size_t size,
				cl::Buffer& windows, const size_t windowsSize,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void filterNonpositive(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA, cl::Buffer& matrixB,
				cl::Buffer& sizes, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void filterZeros(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA, cl::Buffer& matrixB,
				cl::Buffer& sizes, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void filterInfs(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA, cl::Buffer& matrixB,
				cl::Buffer& sizes, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void filterInfs(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA,
				cl::Buffer& sizes, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

	private:

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		// Źródła kerneli OpenCL
		static std::vector<std::string> ssources;

		/** Kernele OCL */

		cl::Kernel generateWavelengthsMatrixKernel;
		static const char* generateWavelengthsMatrixKernelName;

		cl::Kernel addSpectrumKernel;
		static const char* addSpectrumKernelName;

		cl::Kernel filterWithWavelengthWindowsKernel;
		static const char* filterWithWavelengthWindowsKernelName;

		cl::Kernel filterNonpositiveKernel;
		static const char* filterNonpositiveKernelName;

		cl::Kernel filterZerosKernel;
		static const char* filterZerosKernelName;

		cl::Kernel filterInfsKernel;
		static const char* filterInfsKernelName;

		/** Koniec OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
