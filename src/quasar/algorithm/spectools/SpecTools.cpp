#include "SpecTools.hpp"

namespace algorithm
{

namespace ocl
{

const char* SpecTools::generateWavelengthsMatrixKernelName = "generateWavelengthsMatrix";
const char* SpecTools::addSpectrumKernelName = "addSpectrum";
const char* SpecTools::filterWithWavelengthWindowsKernelName = "filterWithWavelengthWindows";
const char* SpecTools::filterNonpositiveKernelName = "filterNonpositive";
const char* SpecTools::filterZerosKernelName = "filterZeros";
const char* SpecTools::filterInfsKernelName = "filterInfs";

std::vector<std::string> SpecTools::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
SpecTools::SpecTools(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources())
{
	try
	{
		if (this->ssources.empty())
		{
			this->ssources = utils::getSources({ common::get_progdir() + "cl/algorithm/spectools/kernels.cl" });
		}
		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);

		// kernele
		this->generateWavelengthsMatrixKernel = cl::Kernel(this->program, this->generateWavelengthsMatrixKernelName);
		this->addSpectrumKernel = cl::Kernel(this->program, this->addSpectrumKernelName);
		this->filterWithWavelengthWindowsKernel = cl::Kernel(this->program, this->filterWithWavelengthWindowsKernelName);
		this->filterNonpositiveKernel = cl::Kernel(this->program, this->filterNonpositiveKernelName);
		this->filterZerosKernel = cl::Kernel(this->program, this->filterZerosKernelName);
		this->filterInfsKernel = cl::Kernel(this->program, this->filterInfsKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

SpecTools::~SpecTools()
{

}

/**
 * Generuje macierz długości fal dla widm kwazarów.
 *
 * UWAGI:
 *
 * - Macierz wynikowa ma orientację wierszową, tj. w i-tym wierszu macierzy znajdują się długości
 *   fal dla i-tego widm (wymiary macierzy: size x 4096)
 *
 * @param abz_ bufor cl_float4 zawierający współczynniki a, b i z dla każdego obiektu astronomicznego
 * @param size inaczej ilość widm dla których generujemy długości fal.
 * @param wavelengthsMatrix wyjściowy bufor/macierz z długościami fal
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::generateWavelengthsMatrix(cl::Buffer& abz_, const size_t size, cl::Buffer& wavelengthsMatrix,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->generateWavelengthsMatrixKernel.setArg(arg++, sizeof(cl_float4 *), &abz_);
	this->generateWavelengthsMatrixKernel.setArg(arg++, sizeof(cl_float *), &wavelengthsMatrix);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku czasami jest tragiczne.
	cl::NDRange global(size, ASTRO_OBJ_SPEC_SIZE);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->generateWavelengthsMatrixKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Dodaje do każdego widma ze spec podane widmo toAddSpectrum z interpolacją.
 *
 * Interpolacja następuje w zależności od wartości z wavelengthsMatrix i toAddWavelengths,
 * a dodawane wartości są ze spectrumsMatrix i toAddSpectrum.
 *
 * Uwagi:
 *
 * - Obliczenia odbywają się po kolumnach, tj. widmo (długości fali) jednego obiektu
 *   powinno się znajdować w kolumnie macierzy spectrumsMatrix (nie wierszu).
 * - Wynik w buforze outputSpectrumsMatrix również jest ułożony kolumnowo.
 *
 * @param spectrumsMatrix widma kwazarów(macierz).
 * @param wavelengthsMatrix długości fal dla widm ze spectrumsMatrix.
 * @param sizes rozmiary kolejnych widm z buforu spectrumsMatrix.
 * @param size ilość widm w buforze spectrumsMatrix.
 * @param toAddWavelengths długości fal widma, które dodajemy.
 * @param toAddSpectrum widmo, które dodajemy.
 * @param toAddSize rozmiar dodawanego widma.
 * @param outputSpectrumsMatrix bufor z wynikiem.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::addSpectrum(cl::Buffer& spectrumsMatrix, cl::Buffer& wavelengthsMatrix,
		cl::Buffer& sizes, const size_t size,
		cl::Buffer& toAddWavelengths, cl::Buffer& toAddSpectrum, const size_t toAddSize,
		cl::Buffer& outputSpectrumsMatrix,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->addSpectrumKernel.setArg(arg++, sizeof(cl_float *), &wavelengthsMatrix);
	this->addSpectrumKernel.setArg(arg++, sizeof(cl_float *), &spectrumsMatrix);
	this->addSpectrumKernel.setArg(arg++, sizeof(cl_uint *), &sizes);
	this->addSpectrumKernel.setArg(arg++, static_cast<cl_uint>(size));
	this->addSpectrumKernel.setArg(arg++, sizeof(cl_float *), &toAddWavelengths);
	this->addSpectrumKernel.setArg(arg++, sizeof(cl_float *), &toAddSpectrum);
	this->addSpectrumKernel.setArg(arg++, static_cast<cl_uint>(toAddSize));
	this->addSpectrumKernel.setArg(arg++, sizeof(cl_float *), &outputSpectrumsMatrix);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku czasami jest tragiczne.

	size_t global_size_d1 = size;

	// Poprawka jeżeli rows nie dzieli się przez device_max_wg_size.
	size_t remainder = global_size_d1 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if (global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->addSpectrumKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Filtruje dane z każdego widma, tak, że pozostają tylko wartości, których długość fali
 * zawiera się w jakimś z podanych okien.
 *
 * W miejscu wszystkich innych wartości zostaje zapisana wartość INFINITY (nawet tych z pod indeksów
 * przekraczających rozmiar widma).
 *
 * Podane operacje odnoszą się do buforów spectrumsMatrix, wavelengthsMatrix i errorsMatrix.
 *
 * Uwagi:
 *
 * - Obliczenia odbywają się po kolumnach, tj. widmo (długości fali) jednego obiektu
 *   powinno się znajdować w kolumnie macierzy spectrumsMatrix (nie wierszu).
 *
 * @param spectrumsMatrix widma kwazarów.
 * @param wavelengthsMatrix długości fal dla widm ze spectrumsMatrix.
 * @param errorsMatrix błędy pomiaru widm.
 * @param sizes rozmiary kolejnych widm z buforu spectrumsMatrix.
 * @param size ilość widm w buforze spectrumsMatrix.
 * @param windows okna filtrujące (pary długości fal)
 * @param windowsSize ilość okien.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::filterWithWavelengthWindows(cl::Buffer& spectrumsMatrix, cl::Buffer& wavelengthsMatrix, cl::Buffer& errorsMatrix,
		cl::Buffer& sizes, const size_t size,
		cl::Buffer& windows, const size_t windowsSize,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->filterWithWavelengthWindowsKernel.setArg(arg++, sizeof(cl_float *), &wavelengthsMatrix);
	this->filterWithWavelengthWindowsKernel.setArg(arg++, sizeof(cl_float *), &spectrumsMatrix);
	this->filterWithWavelengthWindowsKernel.setArg(arg++, sizeof(cl_float *), &errorsMatrix);
	this->filterWithWavelengthWindowsKernel.setArg(arg++, sizeof(cl_uint *), &sizes);
	this->filterWithWavelengthWindowsKernel.setArg(arg++, sizeof(cl_float2 *), &windows);
	this->filterWithWavelengthWindowsKernel.setArg(arg++, static_cast<cl_uint>(windowsSize));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	cl::NDRange global(size, ASTRO_OBJ_SPEC_SIZE);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->filterWithWavelengthWindowsKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Filtruje dane tak, że pozostają tylko wartości dla których
 * odpowiadająca wartość ze spectrumsMatrix jest większa od zera (filtruje również spectrumsMatrix).
 *
 * W miejscu wszystkich innych wartości zostaje zapisana wartość INFINITY (nawet tych z pod indeksów
 * przekraczających rozmiar widma).
 *
 * Uwagi:
 *
 * - Obliczenia odbywają się po kolumnach, tj. widmo (długości fali) jednego obiektu
 *   powinno się znajdować w kolumnie macierzy spectrumsMatrix (nie wierszu).
 *
 * @param spectrumsMatrix widma kwazarów.
 * @param matrixA dowolny bufor wielkości spectrumsMatrix
 * @param matrixB dowolny bufor wielkości spectrumsMatrix
 * @param sizes rozmiary kolejnych widm z buforu spectrumsMatrix.
 * @param size ilość widm w buforze spectrumsMatrix.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::filterNonpositive(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA, cl::Buffer& matrixB,
		cl::Buffer& sizes, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->filterNonpositiveKernel.setArg(arg++, sizeof(cl_float *), &spectrumsMatrix);
	this->filterNonpositiveKernel.setArg(arg++, sizeof(cl_float *), &matrixA);
	this->filterNonpositiveKernel.setArg(arg++, sizeof(cl_float *), &matrixB);
	this->filterNonpositiveKernel.setArg(arg++, sizeof(cl_uint *), &sizes);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();

	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	cl::NDRange global(size, ASTRO_OBJ_SPEC_SIZE);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->filterNonpositiveKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Filtruje dane tak, że pozostają tylko wartości, których odpowiadające
 * wartości ze spectrumsMatrix są różne od zera (filtruje również spectrumsMatrix).
 *
 * Uwagi:
 *
 * - Obliczenia odbywają się po kolumnach, tj. widmo (długości fali) jednego obiektu
 *   powinno się znajdować w kolumnie macierzy spectrumsMatrix (nie wierszu).
 *
 * @param spectrumsMatrix widma kwazarów.
 * @param matrixA dowolny bufor wielkości spectrumsMatrix
 * @param matrixB dowolny bufor wielkości spectrumsMatrix
 * @param sizes rozmiary kolejnych widm z buforu spectrumsMatrix.
 * @param size ilość widm w buforze spectrumsMatrix.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::filterZeros(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA, cl::Buffer& matrixB,
		cl::Buffer& sizes, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->filterZerosKernel.setArg(arg++, sizeof(cl_float *), &spectrumsMatrix);
	this->filterZerosKernel.setArg(arg++, sizeof(cl_float *), &matrixA);
	this->filterZerosKernel.setArg(arg++, sizeof(cl_float *), &matrixB);
	this->filterZerosKernel.setArg(arg++, sizeof(cl_uint *), &sizes);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();

	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	cl::NDRange global(size, ASTRO_OBJ_SPEC_SIZE);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->filterZerosKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Filtruje dane tak, że pozostają tylko wartości, których odpowiadające
 * wartości ze spectrumsMatrix są różne od +/- INIFNITY (filtruje również spectrumsMatrix).
 *
 * Uwagi:
 *
 * - Obliczenia odbywają się po kolumnach, tj. widmo (długości fali) jednego obiektu
 *   powinno się znajdować w kolumnie macierzy spectrumsMatrix (nie wierszu).
 *
 * @param spectrumsMatrix widma kwazarów.
 * @param matrixA dowolny bufor wielkości spectrumsMatrix
 * @param matrixB dowolny bufor wielkości spectrumsMatrix
 * @param sizes rozmiary kolejnych widm ze spectrumsMatrix.
 * @param size ilość widm w buforze spectrumsMatrix.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::filterInfs(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA, cl::Buffer& matrixB,
		cl::Buffer& sizes, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->filterInfsKernel.setArg(arg++, sizeof(cl_float *), &spectrumsMatrix);
	this->filterInfsKernel.setArg(arg++, sizeof(cl_float *), &matrixA);
	this->filterInfsKernel.setArg(arg++, sizeof(cl_float *), &matrixB);
	this->filterInfsKernel.setArg(arg++, sizeof(cl_uint *), &sizes);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();

	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	cl::NDRange global(size, ASTRO_OBJ_SPEC_SIZE);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->filterInfsKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 * Filtruje dane tak, że pozostają tylko wartości, których odpowiadające
 * wartości ze spectrumsMatrix są różne od +/- INIFNITY (filtruje również spectrumsMatrix).
 *
 * Uwagi:
 *
 * - Obliczenia odbywają się po kolumnach, tj. widmo (długości fali) jednego obiektu
 *   powinno się znajdować w kolumnie macierzy spectrumsMatrix (nie wierszu).
 *
 * @param spectrumsMatrix widma kwazarów.
 * @param matrixA dowolny bufor wielkości spectrumsMatrix
 * @param sizes rozmiary kolejnych widm ze spectrumsMatrix.
 * @param size ilość widm w buforze spectrumsMatrix.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void SpecTools::filterInfs(cl::Buffer& spectrumsMatrix, cl::Buffer& matrixA,
		cl::Buffer& sizes, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->filterInfsKernel.setArg(arg++, sizeof(cl_float *), &spectrumsMatrix);
	this->filterInfsKernel.setArg(arg++, sizeof(cl_float *), &matrixA);
	this->filterInfsKernel.setArg(arg++, sizeof(cl_float *), nullptr);
	this->filterInfsKernel.setArg(arg++, sizeof(cl_uint *), &sizes);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();

	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	cl::NDRange global(size, ASTRO_OBJ_SPEC_SIZE);
	cl::NDRange local(1, device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>());

	this->queue.enqueueNDRangeKernel(
			this->filterInfsKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

} /* namespace ocl */

} /* namespace algorithm */

