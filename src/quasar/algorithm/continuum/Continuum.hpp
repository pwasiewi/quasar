#pragma once

#include "../defines.hpp"

#include "../utils.hpp"
#include "../spectools/SpecTools.hpp"
#include "../tools/Tools.hpp"
#include "../basicmatrix/BasicMatrix.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <algorithm>

namespace algorithm
{

namespace ocl
{

/**
 * Klasa pozwalająca na obliczenie continuum dla widm z podanej macierzy widm.
 *
  * UWAGI:
 *
 * - Metody tej klasy zakładają, że maksymalna długość widma wynosi 4096 elementów (patrz definicja ASTRO_OBJ_SPEC_SIZE),
 * 	 a podawane bufory cl::Buffer są rozmiaru N x 4096 (N - ilość widm).
 * - Obliczanie działa na macierzach, w których widma / długości fal widm / błędów pomiaru
 *   są ułożone kolumnowo (kolumn tyle ile widm, 4096 wierszy).
 *
 */
class Continuum
{
	public:

		/**
		 * Struktura dla zwracania wyników
		 */
		struct Results
		{
				// Kontinuum
				cl::Buffer dcontinuumsMatrix;
				cl::Buffer continuumsMatrix;
				// Dopasowanie
				std::vector<cl_float> chisqs;
				// Parametry prostej regresji
				std::vector<cl_float8> c_reglin_result;
				// Parametry prostej regresji
				std::vector<cl_float8> reglin_result;
		};

		Continuum(const cl::Context&, const cl::CommandQueue&);
		virtual ~Continuum();

		Continuum::Results run(cl::Buffer& spectrumsMatrix, cl::Buffer& wavelengthsMatrix, cl::Buffer& errorsMatrix,
				cl::Buffer& sizes, const size_t size,
				std::vector<cl_float2> windows,
				cl_float ampWavelength,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

	private:

		void fixReglinResults(cl::Buffer& c_reglin_results, cl::Buffer& reglin_results, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void calcCfunDcfun(cl::Buffer& wavelengthsMatrix, cl::Buffer& dcontinuumsMatrix, cl::Buffer& continuumsMatrix,
				const size_t size,
				cl::Buffer& c_reglin_results, cl::Buffer& reglin_results,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void calcCw(cl::Buffer& wavelengthsMatrix_filtered, cl::Buffer& continuumsMatrix_filtered,
				const size_t max_spec_filtered_size, const size_t size,
				cl::Buffer& c_reglin_results,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void reduceChisqs(cl::Buffer& chisqs,
				cl::Buffer& sizes_filtered, const size_t size,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		static std::vector<std::string> ssources;

		/** Narzędzia */

		Tools tools;
		SpecTools specTools;
		BasicMatrix basicMatrix;

		/** Kernele OpenCL */

		cl::Kernel fixReglinResultsKernel;
		static const char* fixReglinResultsKernelName;

		cl::Kernel calcCfunDcfunKernel;
		static const char* calcCfunDcfunKernelName;

		cl::Kernel calcCwKernel;
		static const char* calcCwKernelName;

		cl::Kernel reduceChisqsKernel;
		static const char* reduceChisqsKernelName;

		/** End OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
