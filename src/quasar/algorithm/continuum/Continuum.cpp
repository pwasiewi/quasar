#include "Continuum.hpp"

namespace algorithm
{

namespace ocl
{

const char* Continuum::fixReglinResultsKernelName = "fix_reglin_results";
const char* Continuum::calcCfunDcfunKernelName = "calc_cfun_dcfun";
const char* Continuum::calcCwKernelName = "calc_cw";
const char* Continuum::reduceChisqsKernelName = "reduce_chisqs";

std::vector<std::string> Continuum::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
Continuum::Continuum(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources()),
			tools(Tools(context, queue)),
			specTools(SpecTools(context, queue)),
			basicMatrix(BasicMatrix(context, queue))
{
	try
	{
		if(queue.getInfo<CL_QUEUE_PROPERTIES>() & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)
		{
			throw std::logic_error("OpenCL command queue is out-of-order; in-order expected.");
		}

		if (this->ssources.empty())
		{
			this->ssources = utils::getSources({ common::get_progdir() + "cl/algorithm/continuum/kernels.cl" });
		}
		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);

		// kernele
		this->fixReglinResultsKernel = cl::Kernel(this->program, this->fixReglinResultsKernelName);
		this->calcCfunDcfunKernel = cl::Kernel(this->program, this->calcCfunDcfunKernelName);
		this->calcCwKernel = cl::Kernel(this->program, this->calcCwKernelName);
		this->reduceChisqsKernel = cl::Kernel(this->program, this->reduceChisqsKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

Continuum::~Continuum()
{

}

/**
 * Oblicza continuum dla każdego widma z macierzy spectrumsMatrix. Widma znajdują się w kolejnych
 * kolumnach macierzy spectrumsMatrix. Wyniki również są zapisane "po kolumnach".
 *
 * Modyfikuje bufory spectrumsMatrix, wavelengthsMatrix i errorsMatrix zapisując
 * w nich wyniki obliczeń.
 *
 * 		spectrumsMatrix		-> continuumMatrix (macierz continuum dla każdego widma)
 * 		wavelengthsMatrix	-> dcontinuumMatrix
 * 		errorsMatrix		-> chisqs (wektor z jakością dopasowania (sprawdzaną metodą najmniejszy kwadratów)
 *							  	między widem a jego continuum w podanych oknach filtrujących)
 *
 * @param spectrumsMatrix widma kwazarów. (Macierz w której kolumny to kolejne widma, a maksymalna długość widma jest nie większa niż 4096).
 * @param wavelengthsMatrix długości fal dla widm ze specs.
 * @param errorsMatrix błędy pomiaru widm.
 * @param sizes rozmiary kolejnych widm z buforu specs.
 * @param size ilość widm w buforze specs.
 * @param windows okna filtrujące (pary długości fal)
 * @param ampWavelength długość fali na jakiej podana będzie amplituda funkcji
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
Continuum::Results Continuum::run(
		cl::Buffer& spectrumsMatrix, cl::Buffer& wavelengthsMatrix, cl::Buffer& errorsMatrix,
		cl::Buffer& sizes, const size_t size,
		std::vector<cl_float2> windows,
		cl_float ampWavelength,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	if (events != nullptr)
	{
		cl::Event::waitForEvents(*events);
	}

	// Zapamiętuje wersje oryginalną, bo będzie potrzebna.
	cl::Buffer wavelengthsMatrix_original = utils::make_qbuffer_copy(context, queue, CL_MEM_READ_WRITE, size, wavelengthsMatrix);

	// ________________________________________________________________________ //
	// __filtrowanie na podstawie podanych okien_______________________________ //
	// ________________________________________________________________________ //

	// Bufor na okna
	auto windowsBuffer = utils::make_buffer<cl_float2>(context, CL_MEM_READ_ONLY, windows.size());
	cl::copy(queue, windows.begin(), windows.end(), windowsBuffer);
	//
	specTools.filterWithWavelengthWindows(spectrumsMatrix, wavelengthsMatrix, errorsMatrix, sizes, size, windowsBuffer, windows.size());

	// ________________________________________________________________________ //
	// __usuwanie niefizycznych do logarytmowania______________________________ //
	// ________________________________________________________________________ //

	// Usuwam wartości <= 0, żeby móc logarytmować.
	specTools.filterNonpositive(spectrumsMatrix, wavelengthsMatrix, errorsMatrix, sizes, size);

	// ________________________________________________________________________ //
	// __tworzenie buforów dla przefiltrowanych danych_________________________ //
	// ________________________________________________________________________ //

	// Rozmiary widm po filtrowaniu.
	cl::Buffer sizes_filtered = utils::make_buffer<cl_uint>(context, CL_MEM_READ_WRITE, size);
	// Zliczanie elementów po przefiltrowaniu.
	tools.countIfNotInf(spectrumsMatrix, size, QUASAR_DATA_SIZE, sizes_filtered);

	// Pobranie rozmiarów i wybranie największego (max), następnie
	// obliczenie najbliżej wielokrotności 64 większej od maksimum.
	// Potrzebne, żeby adresy elementów początkowych były wielokrotnością 64;
	std::vector<cl_uint> sizes_filtered_vec(size, 0);
	cl::copy(queue, sizes_filtered, sizes_filtered_vec.begin(), sizes_filtered_vec.end());
	auto max = *std::max_element(sizes_filtered_vec.begin(), sizes_filtered_vec.end());

	size_t max_spec_filtered_size = max > 0 ? max : 64;
	size_t remainder = max_spec_filtered_size % 64;
	if(remainder != 0)
	{
		max_spec_filtered_size += 64 - remainder;
	}
	if(max_spec_filtered_size < max)
	{
		throw std::runtime_error("Error in calculating buffer size.");
	}

	// Bufory na przefiltrowana dane.
	cl::Buffer spectrumsMatrix_filtered = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, max_spec_filtered_size * size);
	cl::Buffer wavelengthsMatrix_filtered = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, max_spec_filtered_size * size);
	// Nie potrzeba rezerwować nowego buforu dla errors_filtered, patrz niżej.
//	cl::Buffer errorsMatrix_filtered = utils::make_buffer<cl_float>(context, CL_MEM_READ_WRITE, max_spec_filtered_size * size);

	tools.copyIfNotInf(spectrumsMatrix, size, QUASAR_DATA_SIZE, spectrumsMatrix_filtered, max_spec_filtered_size); //
	tools.copyIfNotInf(wavelengthsMatrix, size, QUASAR_DATA_SIZE, wavelengthsMatrix_filtered , max_spec_filtered_size);


	// Dane z buforów spectrumsMatrix, wavelengthsMatrix nie są już potrzebne,
	// więc można ich do użyć do innych danych
	//
	// Wolne: spectrumsMatrix, wavelengthsMatrix

	cl::Buffer errorsMatrix_filtered = spectrumsMatrix;
	tools.copyIfNotInf(errorsMatrix, size, QUASAR_DATA_SIZE, errorsMatrix_filtered , max_spec_filtered_size);

	//
	// Wolne bufory: wavelengthsMatrix, errorsMatrix

	// ________________________________________________________________________ //
	// __tworzenie kopii przefiltrowanych danych (bez błędów)__________________ //
	// ________________________________________________________________________ //

	auto spectrumsMatrix_filtered_copy = errorsMatrix;
	auto wavelengthsMatrix_filtered_copy = wavelengthsMatrix;

	// Zapisanie kopii buforów _filtered
	size_t byteSize = sizeof(cl_float) * max_spec_filtered_size * size;
	queue.enqueueCopyBuffer(spectrumsMatrix_filtered, spectrumsMatrix_filtered_copy, 0, 0, byteSize);
	queue.enqueueCopyBuffer(wavelengthsMatrix_filtered, wavelengthsMatrix_filtered_copy, 0, 0, byteSize);
//	queue.enqueueCopyBuffer(errorsMatrix_filtered, errors, 0, 0, byteSize); // Nie potrzeba kopii.

	//
	// Wolne bufory: brak

	// ________________________________________________________________________ //
	// __wyznacznie "parametrów continuum" cpar________________________________ //
	// ________________________________________________________________________ //

	basicMatrix.log10(spectrumsMatrix_filtered_copy, size, max_spec_filtered_size);
	basicMatrix.log10(wavelengthsMatrix_filtered_copy, size, max_spec_filtered_size);

	// bufor na wyniki dopasowania prostej regresji (czyli jej współczynniki)
	// dla przefiltrowanych danych.
	auto c_reglin_results = utils::make_buffer<cl_float8>(context, CL_MEM_READ_WRITE, size);
	tools.reglin(wavelengthsMatrix_filtered_copy, spectrumsMatrix_filtered_copy, size, max_spec_filtered_size, sizes_filtered, c_reglin_results);

	// ________________________________________________________________________ //
	// ________________________________________________________________________ //
	// ________________________________________________________________________ //

	if(ampWavelength > std::pow(FLT_MIN, 10.0001f)) // Odrzucam takie lamp, że wynik log10(lamp) będzie równy -INFINITY
	{
		cl_float lampLog10 = static_cast<cl_float>(std::log10(static_cast<float>(ampWavelength)));
		basicMatrix.minus(wavelengthsMatrix_filtered_copy, size, max_spec_filtered_size, lampLog10);
	}

	// bufor na wyniki dopasowania prostej regresji (czyli jej współczynniki)
	// dla przefiltrowanych danych.
	auto reglin_results = utils::make_buffer<cl_float8>(context, CL_MEM_READ_WRITE, size);
	tools.reglin(wavelengthsMatrix_filtered_copy, spectrumsMatrix_filtered_copy, size, max_spec_filtered_size, sizes_filtered, reglin_results);

	// wavelengths_filtered_copy i spectrums_filtered_copy już nie są potrzebne, więc
	// bufory errorsMatrix i wavelengthsMatrix mogą być użyte do innych danych.

	//
	// Wolne bufory: errorsMatrix, wavelengthsMatrix.


	// Przeliczenie parametrów i błędów w przestrzeni widma.
	// Modyfikacja współczynnika b w c_reglin_results: b = 10^b.
	fixReglinResults(c_reglin_results, reglin_results, size);

	//
	auto continuumsMatrix_filtered = wavelengthsMatrix_filtered; // Zapisuje w tym samym buforze, z którego biorę dane (kernel na to pozwala).
	calcCw(wavelengthsMatrix_filtered, continuumsMatrix_filtered, max_spec_filtered_size, size, c_reglin_results);

	auto reducedChisqs_filtered = errorsMatrix; // To co jest w errors nie będzie więcej potrzebne.
	tools.chisq(spectrumsMatrix_filtered, continuumsMatrix_filtered, errorsMatrix_filtered, size, max_spec_filtered_size, sizes_filtered, reducedChisqs_filtered);
	reduceChisqs(reducedChisqs_filtered, sizes_filtered, size);

	// ________________________________________________________________________ //
	// ___obliczanie continuum i błędów continuum______________________________ //
	// ________________________________________________________________________ //

	//
	// errorsMatrix_filtered niepotrzebne.
	// Wolne: wavelengthsMatrix, spectrumsMatrix

	//
	auto dcontinuumsMatrix = wavelengthsMatrix; // To co jest w lambdas nie będzie więcej potrzebne.
	auto continuumsMatrix = spectrumsMatrix; // Jak wyżej.
	calcCfunDcfun(wavelengthsMatrix_original, dcontinuumsMatrix, continuumsMatrix, size, c_reglin_results, reglin_results, nullptr, event);

	// ________________________________________________________________________ //
	// ___kopiowanie wyników na host___________________________________________ //
	// ________________________________________________________________________ //

	std::vector<cl_float> reducedChisqs_filteredVec(size);
	std::vector<cl_float8> reglin_resultsVec(size);
	std::vector<cl_float8> c_reglin_resultsVec(size);

	cl::copy(queue, reducedChisqs_filtered, reducedChisqs_filteredVec.begin(), reducedChisqs_filteredVec.end());
	cl::copy(queue, reglin_results, reglin_resultsVec.begin(), reglin_resultsVec.end());
	cl::copy(queue, c_reglin_results, c_reglin_resultsVec.begin(), c_reglin_resultsVec.end());

	Continuum::Results result
	{
		dcontinuumsMatrix,
		continuumsMatrix,
		reducedChisqs_filteredVec,
		reglin_resultsVec,
		c_reglin_resultsVec
	};
	return result;
}

/**
 *
 * @param c_reglin_results
 * @param reglin_results
 * @param size
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Continuum::fixReglinResults(cl::Buffer& c_reglin_results, cl::Buffer& reglin_results, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->fixReglinResultsKernel.setArg(arg++, sizeof(cl_float8 *), &c_reglin_results);
	this->fixReglinResultsKernel.setArg(arg++, sizeof(cl_float8 *), &reglin_results);
	this->fixReglinResultsKernel.setArg(arg++, static_cast<cl_uint>(size));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = size;
	size_t remainder = global_size_d1 % device_max_wg_size;
	if(remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if(global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->fixReglinResultsKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 *
 * @param wavelengthsMatrix
 * @param dcontinuumsMatrix
 * @param continuumsMatrix
 * @param sizes
 * @param size
 * @param c_reglin_results
 * @param reglin_results
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Continuum::calcCfunDcfun(cl::Buffer& wavelengthsMatrix, cl::Buffer& dcontinuumsMatrix, cl::Buffer& continuumsMatrix,
		const size_t size,
		cl::Buffer& c_reglin_results, cl::Buffer& reglin_results,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->calcCfunDcfunKernel.setArg(arg++, sizeof(cl_float *), &wavelengthsMatrix);
	this->calcCfunDcfunKernel.setArg(arg++, sizeof(cl_float *), &dcontinuumsMatrix);
	this->calcCfunDcfunKernel.setArg(arg++, sizeof(cl_float *), &continuumsMatrix);
	this->calcCfunDcfunKernel.setArg(arg++, sizeof(cl_float8 *), &c_reglin_results);
	this->calcCfunDcfunKernel.setArg(arg++, sizeof(cl_float8 *), &reglin_results);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	cl::NDRange global(size, QUASAR_DATA_SIZE);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->calcCfunDcfunKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 *
 * @param wavelengthsMatrix_filtered
 * @param continuumsMatrix_filtered
 * @param sizes_filtered
 * @param max_spec_filtered_size
 * @param size
 * @param c_reglin_results
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Continuum::calcCw(cl::Buffer& wavelengthsMatrix_filtered, cl::Buffer& continuumsMatrix_filtered,
		const size_t max_spec_filtered_size, const size_t size,
		cl::Buffer& c_reglin_results,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->calcCwKernel.setArg(arg++, sizeof(cl_float *), &wavelengthsMatrix_filtered);
	this->calcCwKernel.setArg(arg++, sizeof(cl_float *), &continuumsMatrix_filtered);
	this->calcCwKernel.setArg(arg++, static_cast<cl_uint>(max_spec_filtered_size));
	this->calcCwKernel.setArg(arg++, sizeof(cl_float8 *), &c_reglin_results);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = max_spec_filtered_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if(remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if(global_size_d2 < max_spec_filtered_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(size, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->calcCwKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 *
 * @param chisqs
 * @param sizes_filtered
 * @param size
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @param event wskaźnik na zdarzenie wykonania tej funkcji.
 */
void Continuum::reduceChisqs(cl::Buffer& chisqs, cl::Buffer& sizes_filtered, const size_t size,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->reduceChisqsKernel.setArg(arg++, sizeof(cl_float *), &chisqs);
	this->reduceChisqsKernel.setArg(arg++, sizeof(cl_uint *), &sizes_filtered);
	this->reduceChisqsKernel.setArg(arg++, static_cast<cl_uint>(size));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = size;
	size_t remainder = global_size_d1 % device_max_wg_size;
	if(remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if(global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->reduceChisqsKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

} /* namespace ocl */

} /* namespace algorithm */

