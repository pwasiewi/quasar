#pragma once

#include "../defines.hpp"

#include "../utils.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>

namespace algorithm
{

namespace ocl
{

class MAVG
{
	public:
		MAVG(const cl::Context&, const cl::CommandQueue&);
		virtual ~MAVG();
		void simpleMAVG(cl::Buffer& in, const size_t width, const size_t height, cl::Buffer& out, const unsigned int window,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void centeredMAVG(cl::Buffer& in, const size_t width, const size_t height, cl::Buffer& cols_heights, cl::Buffer& out, const unsigned int window,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

	private:

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		static std::vector<std::string> ssources;

		cl::Kernel smavgKernel;
		static const char* smavgKernelName;

		cl::Kernel cmavgKernel;
		static const char* cmavgKernelName;

		/** End OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
