#include "MAVG.hpp"

namespace algorithm
{

namespace ocl
{

const char* MAVG::smavgKernelName = "simple_mavg";
const char* MAVG::cmavgKernelName = "centered_mavg";
std::vector<std::string> MAVG::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
MAVG::MAVG(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources())
{
	try
	{
		if (this->ssources.empty())
		{
			this->ssources = utils::getSources({ common::get_progdir() + "cl/algorithm/mavg/kernels.cl" });
		}
		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);

		// kernel
		this->smavgKernel = cl::Kernel(this->program, this->smavgKernelName);
		this->cmavgKernel = cl::Kernel(this->program, this->cmavgKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

MAVG::~MAVG()
{

}

/**
 * Obliczenie SMA dla macierzy po kolumnach.
 *
 * @param in (cl_float) macierz
 * @param width
 * @param height
 * @param out bufor wyjściowy z wynikami MAVG (float).
 * @param window okno MAVG.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem MAVG.
 * @param event wskaźnik na zdarzenie wykonania MAVG.
 */
void MAVG::simpleMAVG(cl::Buffer& in, const size_t width, const size_t height, cl::Buffer& out,
		const unsigned int window, const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->smavgKernel.setArg(arg++, sizeof(cl_float *), &in);
	this->smavgKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->smavgKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->smavgKernel.setArg(arg++, sizeof(cl_float *), &out);
	this->smavgKernel.setArg(arg++, window);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku czasami jest tragiczne.

	size_t global_size_d1 = width;

	// Poprawka jeżeli rows nie dzieli się przez device_max_wg_size.
	size_t remainder = global_size_d1 % device_max_wg_size;
	if(remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if(global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->smavgKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}


/**
 * Obliczenie Centered MA dla macierzy po kolumnach.
 *
 * Centered MA jest średnią kroczącą, dla której wartość średniej kroczącej dla i-tego elementu
 * jest średnią arytmetyczną elementów o indeksach z przedziału (i - window_width; i + window_width).
 *
 * @param in (cl_float) macierz
 * @param width
 * @param height
 * @param cols_heights bufor z ilością znaczących elementów każdej kolumnie (tylko znaczące elementy są brane pod uwagę przy obliczaniu średniej)
 * @param out bufor wyjściowy z wynikami MAVG (float).
 * @param window okno MAVG.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem MAVG.
 * @param event wskaźnik na zdarzenie wykonania MAVG.
 */
void MAVG::centeredMAVG(cl::Buffer& in, const size_t width, const size_t height, cl::Buffer& cols_heights, cl::Buffer& out,
		const unsigned int window, const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->cmavgKernel.setArg(arg++, sizeof(cl_float *), &in);
	this->cmavgKernel.setArg(arg++, static_cast<cl_uint>(width));
	this->cmavgKernel.setArg(arg++, static_cast<cl_uint>(height));
	this->cmavgKernel.setArg(arg++, sizeof(cl_uint *), &cols_heights);
	this->cmavgKernel.setArg(arg++, sizeof(cl_float *), &out);
	this->cmavgKernel.setArg(arg++, window);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku czasami jest tragiczne.

	size_t global_size_d1 = width;

	// Poprawka jeżeli rows nie dzieli się przez device_max_wg_size.
	size_t remainder = global_size_d1 % device_max_wg_size;
	if(remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if(global_size_d1 < width)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->cmavgKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

} /* namespace ocl */

} /* namespace algorithm */

