#include "SpecParameterization.hpp"

namespace algorithm
{

namespace ocl
{

const char* SpecParameterization::fitGaussianKernelName = "fit_gaussian";
const char* SpecParameterization::calcGaussianKernelName = "calc_gaussian";
const char* SpecParameterization::calcGaussianChisqKernelName = "calc_gaussian_chisq";
const char* SpecParameterization::calcGaussianFWHMKernelName = "calc_gaussian_fwhm";

std::vector<std::string> SpecParameterization::ssources;

/**
 *
 * @param context kontekst OpenCL.
 * @param queue kolejka poleceń OpenCL.
 */
SpecParameterization::SpecParameterization(const cl::Context& context, const cl::CommandQueue& queue) :
			context(context),
			queue(queue),
			sources(cl::Program::Sources()),
			tools(Tools(context, queue)),
			specTools(SpecTools(context, queue)),
			basicMatrix(BasicMatrix(context, queue)),
			mavg(MAVG(context, queue)),
			ctm(Continuum(context, queue)),
			fefit(FeFitWin(context, queue))
{
	try
	{
		if (queue.getInfo<CL_QUEUE_PROPERTIES>() & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)
		{
			throw std::logic_error("OpenCL command queue is out-of-order; in-order expected.");
		}

		if (this->ssources.empty())
		{
			this->ssources = utils::getSources(
					{ common::get_progdir() + "cl/algorithm/specparameterization/kernels.cl" });
		}

		for (auto& s : this->ssources)
		{
			this->sources.push_back({ s.c_str(), s.length() });
		}
		// Zbudowanie programu
		std::vector<cl::Device> devices = { queue.getInfo<CL_QUEUE_DEVICE>() };
		this->program = cl::Program(context, this->sources);
		this->program.build(devices);
		// kernele
		this->fitGaussianKernel = cl::Kernel(this->program, this->fitGaussianKernelName);
		this->calcGaussianKernel = cl::Kernel(this->program, this->calcGaussianKernelName);
		this->calcGaussianChisqKernel = cl::Kernel(this->program, this->calcGaussianChisqKernelName);
		this->calcGaussianFWHMKernel = cl::Kernel(this->program, this->calcGaussianFWHMKernelName);
	} catch (const cl::Error &e)
	{
		CHECK_BUILD_PROGRAM_FAILURE(this->queue, this->program);
		std::rethrow_exception(std::current_exception());
	} catch (...)
	{
		std::rethrow_exception(std::current_exception());
	}
}

SpecParameterization::~SpecParameterization()
{

}

/**
 *
 * @param astronomicalObjs obiekty astronomiczny do przeanalizowania.
 * @param elements elementy, których dopasowania szukamy w widmach obiektów.
 * @param continuumWindows okna widmowe do dopasowania kontinuum (zakresy długości fal w A).
 * @param ampWavelength długość fali na jakiej podana będzie amplituda funkcji.
 * @param feWindows okna zdominowane przez emisje żelaza (zakresy długości fal w A).
 * @param feTemplate szablon żelaza.
 * @param fitParameteres parametry dopasowania szablonu żelaza.
 * @param events wskaźnik na zdarzenia, na które należy czekać przed wykonaniem tej funkcji.
 * @return wyniki parametryzacji obiektów astronomicznych.
 */
SpecParameterization::Results SpecParameterization::run(std::vector<Quasar> astronomicalObjs,
		std::vector<SpectralLine> elements,
		std::vector<cl_float2> continuumWindows, cl_float ampWavelength,
		std::vector<cl_float2> feWindows, FeTemplate feTemplate,
		FeFitWin::FitParameters fitParameteres,
		const std::vector<cl::Event>* events)
{
	if (events != nullptr)
	{
		cl::Event::waitForEvents(*events);
	}

	size_t size = astronomicalObjs.size();

	// ________________________________________________________________________ //
	// __bufory ze danymi______________________________________________________ //
	// ________________________________________________________________________ //

	cl::Buffer spectrumsMatrix = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	cl::Buffer wavelengthsMatrix = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	cl::Buffer errorsMatrix = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	cl::Buffer sizes = utils::make_buffer<cl_uint>(context, CL_MEM_READ_WRITE, size);
	cl::Buffer continuumsMatrix;
	cl::Buffer feTemplatesMatrix;

	// ________________________________________________________________________ //
	// __bufory ze danymi______________________________________________________ //
	// ________________________________________________________________________ //

	std::vector<cl_float> spectrumsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size);
	std::vector<cl_float> wavelengthsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size);
	std::vector<cl_float> errorsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size);
	std::vector<cl_float> continuumsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size);
	std::vector<cl_float> dcontinuumsMatrixHost(ASTRO_OBJ_SPEC_SIZE * size);
	std::vector<cl_float> feTemplatesMatrixHost(ASTRO_OBJ_SPEC_SIZE * size);
	std::vector<cl_uint> sizesHost;

	// ________________________________________________________________________ //
	// __zczytanie rozmiarów widm kolejnych obiektów___________________________ //
	// ________________________________________________________________________ //

	sizesHost = utils::sizes(astronomicalObjs);

	// ________________________________________________________________________ //
	// __generowanie długości fal______________________________________________ //
	// ________________________________________________________________________ //
	{
		std::vector<cl_float4> abz_;
		for (auto& obj : astronomicalObjs)
		{
			abz_.push_back({ obj.getParams()->a, obj.getParams()->b, obj.getParams()->z, 0.0f });
		}

		auto abz_buffer = utils::make_buffer(context, CL_MEM_READ_ONLY, abz_);
		auto temp = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		cl::copy(queue, abz_.begin(), abz_.end(), abz_buffer);
		specTools.generateWavelengthsMatrix(abz_buffer, size, temp);
		basicMatrix.transpose(temp, ASTRO_OBJ_SPEC_SIZE, size, wavelengthsMatrix);
	}

	// ________________________________________________________________________ //
	// __kopiowanie danych do buforów__________________________________________ //
	// ________________________________________________________________________ //

	utils::copy(queue, astronomicalObjs, spectrumsMatrix, errorsMatrix);
	cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);

	// ________________________________________________________________________ //
	// __transpozycja danych (i wygładzenie)___________________________________ //
	// ________________________________________________________________________ //
	{
		auto temp = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
		basicMatrix.transpose(spectrumsMatrix, ASTRO_OBJ_SPEC_SIZE, size, temp);
		mavg.centeredMAVG(temp, size, ASTRO_OBJ_SPEC_SIZE, sizes, spectrumsMatrix, 10);

		basicMatrix.transpose(errorsMatrix, ASTRO_OBJ_SPEC_SIZE, size, temp);
		errorsMatrix = temp;
	}

	// ________________________________________________________________________ //
	// __filtrowanie wartości, dla których błąd wynosi 0_______________________ //
	// ________________________________________________________________________ //
	{
		// filtrowanie
		specTools.filterZeros(errorsMatrix, spectrumsMatrix, wavelengthsMatrix, sizes, size);
		// ustalenie nowych rozmiarów
		tools.countIfNotInf(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, sizes);

		// kopiowanie przefiltrowanych danych
		tools.copyIfNotInf(errorsMatrix, size, ASTRO_OBJ_SPEC_SIZE, errorsMatrix, ASTRO_OBJ_SPEC_SIZE);
		tools.copyIfNotInf(wavelengthsMatrix, size, ASTRO_OBJ_SPEC_SIZE, wavelengthsMatrix, ASTRO_OBJ_SPEC_SIZE);
		tools.copyIfNotInf(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, spectrumsMatrix, ASTRO_OBJ_SPEC_SIZE);
	}

	// ________________________________________________________________________ //
	// __kopiowanie transponowanych, wygładzonych, przefiltrowanych danych_____ //
	// __na pamięć hosta_______________________________________________________ //
	// ________________________________________________________________________ //

	cl::copy(queue, sizes, sizesHost.begin(), sizesHost.end());
	cl::copy(queue, spectrumsMatrix, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end());
	cl::copy(queue, wavelengthsMatrix, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end());
	cl::copy(queue, errorsMatrix, errorsMatrixHost.begin(), errorsMatrixHost.end());

	Continuum::Results continuumResults;
	FeFitWin::Results feResults;

	//
	int max_i = 3;
	for (int i = 0; i < max_i; i++)
	{

		// ________________________________________________________________________ //
		// __obliczenie kontinuum__________________________________________________ //
		// ________________________________________________________________________ //

		continuumResults = ctm.run(spectrumsMatrix, wavelengthsMatrix, errorsMatrix, sizes, size, continuumWindows,
				ampWavelength);

		continuumsMatrix = continuumResults.continuumsMatrix;

		// ________________________________________________________________________ //
		// __kopiowanie kontinuum na host__________________________________________ //
		// ________________________________________________________________________ //

		cl::copy(queue, continuumsMatrix, continuumsMatrixHost.begin(), continuumsMatrixHost.end());
		// Błędy kontinuum NIE są UŻYWANE
		//
		// Jeżeli ostatnia pętla to trzeba zapisać błędy kontinuum na host
//		if (i == (max_i - 1))
//		{
//			cl::copy(queue, continuumResult.dcontinuumsMatrix,
//					dcontinuumsMatrixHost.begin(), dcontinuumsMatrixHost.end());
//		}

		// ________________________________________________________________________ //
		// __wczytywanie do buforów oryginalnych danych____________________________ //
		// ________________________________________________________________________ //

		// Trzeba zadeklarować nowy bufor, bo poprzedni zajęła macierz continuumsMatrix
		spectrumsMatrix = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);

		cl::copy(queue, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end(), spectrumsMatrix);
		cl::copy(queue, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end(), wavelengthsMatrix);
		cl::copy(queue, errorsMatrixHost.begin(), errorsMatrixHost.end(), errorsMatrix);
		cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);

		// ________________________________________________________________________ //
		// __dopasowanie do szablonu żelaza________________________________________ //
		// ________________________________________________________________________ //

		FeFitWin::Data specData = {
				spectrumsMatrixHost, wavelengthsMatrixHost,
				errorsMatrixHost, continuumsMatrixHost, sizesHost
		};

		FeFitWin::Buffers specBuffs = {
				spectrumsMatrix, wavelengthsMatrix,
				errorsMatrix, continuumsMatrix, sizes
		};

		feResults = fefit.run(specData, specBuffs, size, feTemplate, feWindows, fitParameteres);

		if (i < (max_i - 1))
		{
			cl::copy(queue, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end(), spectrumsMatrix);
			cl::copy(queue, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end(), wavelengthsMatrix);
			cl::copy(queue, errorsMatrixHost.begin(), errorsMatrixHost.end(), errorsMatrix);
			cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);

			basicMatrix.minus(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, feResults.feTemplateMatrix, spectrumsMatrix);
		}

		// Jeżeli ostatnia pętla to trzeba zapisać feTemplateMatrix
		if (i == (max_i - 1))
		{
			cl::copy(queue, feResults.feTemplateMatrix, feTemplatesMatrixHost.begin(), feTemplatesMatrixHost.end());
		}
	}

	// Błędy kontinuum NIE są UŻYWANE
//	auto dcontinuumsMatrix = utils::make_qbuffer(context, CL_MEM_READ_WRITE, size);
	cl::copy(queue, continuumsMatrixHost.begin(), continuumsMatrixHost.end(), continuumsMatrix);
	// Błędy kontinuum NIE są UŻYWANE
//	cl::copy(queue, dcontinuumsMatrixHost.begin(), dcontinuumsMatrixHost.end(), dcontinuumsMatrix);
	cl::copy(queue, spectrumsMatrixHost.begin(), spectrumsMatrixHost.end(), spectrumsMatrix);
	cl::copy(queue, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end(), wavelengthsMatrix);
	cl::copy(queue, errorsMatrixHost.begin(), errorsMatrixHost.end(), errorsMatrix);
	cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);

	// ________________________________________________________________________ //
	// __obliczenie linii emisyjnych___________________________________________ //
	// ________________________________________________________________________ //

	auto spectrumsEmissionLines = feResults.feTemplateMatrix;
	basicMatrix.minus(spectrumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, feResults.feTemplateMatrix, spectrumsEmissionLines);
	basicMatrix.minus(spectrumsEmissionLines, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, spectrumsEmissionLines);

	// ________________________________________________________________________ //
	// __zapisanie uzyskanych linii emisyjnych_________________________________ //
	// ________________________________________________________________________ //

	auto spectrumsEmissionLinesCopy = spectrumsMatrix;
	queue.enqueueCopyBuffer(spectrumsEmissionLines, spectrumsEmissionLinesCopy, 0, 0,
			sizeof(cl_float) * size * ASTRO_OBJ_SPEC_SIZE);

	// Dopasowanie do elementów
	std::vector<SpecParameterization::FitElementResults> fitElementsResults;
	for (auto& element : elements)
	{
		auto fit = fitElement(spectrumsEmissionLines, spectrumsEmissionLinesCopy,
				continuumsMatrix, /* dcontinuumsMatrix, */
				wavelengthsMatrix, wavelengthsMatrixHost, errorsMatrix,
				sizes, sizesHost, size, element);
		fitElementsResults.push_back(fit);

		// Kopiowanie pełnych danych, żeby można ich użyć dla kolejnego elementu
		// w kolejnej iteracji pętli.

		queue.enqueueCopyBuffer(spectrumsEmissionLinesCopy, spectrumsEmissionLines, 0, 0,
				sizeof(cl_float) * size * ASTRO_OBJ_SPEC_SIZE);
		cl::copy(queue, continuumsMatrixHost.begin(), continuumsMatrixHost.end(), continuumsMatrix);
		// Błędy kontinuum NIE są UŻYWANE
//		cl::copy(queue, dcontinuumsMatrixHost.begin(), dcontinuumsMatrixHost.end(), dcontinuumsMatrix);
		// Nie potrzeba kopiować, bo fitElement() pozostawia nienaruszony ten bufor.
//		cl::copy(queue, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end(), wavelengthsMatrix);
		// Nie potrzeba kopiować, bo fitElement() pozostawia nienaruszony ten bufor.
//		cl::copy(queue, errorsMatrixHost.begin(), errorsMatrixHost.end(), errorsMatrix);
		// Nie potrzeba kopiować, bo fitElement() pozostawia nienaruszony ten bufor.
//		cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);
	}

	SpecParameterization::Results results = {
			continuumResults.chisqs, continuumResults.c_reglin_result, continuumResults.reglin_result,

			feResults.scaleRates,
			feResults.sizes_fewindows, feResults.reducedChisqs_fewindows,
			feResults.reducedChisqs_full, feResults.ews_full,
			feResults.reducedChisqs_feRange, feResults.ews_feRange,

			fitElementsResults
	};
	return results;
}

/**
 *
 * @param spectrumsEmissionLines
 * @param spectrumsEmissionLinesCopy
 * @param continuumsMatrix
 * @param wavelengthsMatrix
 * @param wavelengthsMatrixHost
 * @param errorsMatrix
 * @param sizes
 * @param size
 * @param element
 * @return
 */
SpecParameterization::FitElementResults SpecParameterization::fitElement(
		cl::Buffer& spectrumsEmissionLines, cl::Buffer& spectrumsEmissionLinesCopy,
		cl::Buffer& continuumsMatrix, /*cl::Buffer& dcontinuumsMatrix, błędy continuum nie są używane */
		cl::Buffer& wavelengthsMatrix, std::vector<cl_float> wavelengthsMatrixHost,
		cl::Buffer& errorsMatrix,
		cl::Buffer& sizes, std::vector<cl_uint> sizesHost, size_t size, SpectralLine element)
{
	// ________________________________________________________________________ //
	// __filtrowanie danych dla zakresu elementu_______________________________ //
	// ________________________________________________________________________ //

	std::vector<cl_float2> feFitRangeVec(1, element.range);

	auto feFitRangeBuffer = utils::make_buffer<cl_float2>(context, CL_MEM_READ_ONLY, feFitRangeVec.size());
	cl::copy(queue, feFitRangeVec.begin(), feFitRangeVec.end(), feFitRangeBuffer);

	specTools.filterWithWavelengthWindows(spectrumsEmissionLines, wavelengthsMatrix, continuumsMatrix, sizes, size,
			feFitRangeBuffer, feFitRangeVec.size());
	// Zliczanie elementów po przefiltrowaniu.
	tools.countIfNotInf(spectrumsEmissionLines, size, ASTRO_OBJ_SPEC_SIZE, sizes);

	// ________________________________________________________________________ //
	// __Skracanie danych po filtracji_________________________________________ //
	// ________________________________________________________________________ //

	// Pobranie rozmiarów i wybranie największego (max), następnie
	// obliczenie najbliżej wielokrotności 64 większej od maksimum.
	// Potrzebne, żeby adresy elementów początkowych były wielokrotnością 64;
	std::vector<cl_uint> sizes_filtered_vec(size, 0);
	cl::copy(queue, sizes, sizes_filtered_vec.begin(), sizes_filtered_vec.end());
	auto max = *std::max_element(sizes_filtered_vec.begin(), sizes_filtered_vec.end());

	size_t max_spectrum_filtered_size = max > 0 ? max : 64;
	size_t remainder = max_spectrum_filtered_size % 64;
	if (remainder != 0)
	{
		max_spectrum_filtered_size += 64 - remainder;
	}
	if (max_spectrum_filtered_size < max)
	{
		throw std::runtime_error("Error in calculating buffer size.");
	}

	tools.copyIfNotInf(spectrumsEmissionLines, size, ASTRO_OBJ_SPEC_SIZE, spectrumsEmissionLines,
			max_spectrum_filtered_size);
	tools.copyIfNotInf(wavelengthsMatrix, size, ASTRO_OBJ_SPEC_SIZE, wavelengthsMatrix, max_spectrum_filtered_size);
	tools.copyIfNotInf(continuumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, continuumsMatrix, max_spectrum_filtered_size);
//	tools.copyIfNotInf(dcontinuumsMatrix, size, ASTRO_OBJ_SPEC_SIZE, dcontinuumsMatrix, max_spectrum_filtered_size);

	std::vector<cl_float4> fitGResultsVec(size);
	for (auto& i : fitGResultsVec)
	{
		i = element.fitGuess;
	}

	auto fitGResults = utils::make_buffer(context, CL_MEM_READ_WRITE, fitGResultsVec);
	cl::copy(queue, fitGResultsVec.begin(), fitGResultsVec.end(), fitGResults);

	fitGaussian(spectrumsEmissionLines, wavelengthsMatrix, sizes, size, fitGResults);
	cl::copy(queue, fitGResults, fitGResultsVec.begin(), fitGResultsVec.end());

	// spectrumsEmissionLines już nie jest potrzebne
	auto gaussiansMatrix = spectrumsEmissionLines;
	calcGaussian(wavelengthsMatrix, fitGResults, sizes, size, max_spectrum_filtered_size, gaussiansMatrix);

	// ________________________________________________________________________ //
	// __Gorne oszacowanie błędu na EW korzystając różniczki zupelnej__________ //
	// ________________________________________________________________________ //

	std::vector<cl_float> ewsVec(size);
	{
		auto ews = utils::make_buffer(context, CL_MEM_READ_WRITE, ewsVec);
		basicMatrix.divide(gaussiansMatrix, size, max_spectrum_filtered_size, continuumsMatrix, gaussiansMatrix);
		tools.trapz(gaussiansMatrix, wavelengthsMatrix, size, max_spectrum_filtered_size, sizes, ews);
		cl::copy(queue, ews, ewsVec.begin(), ewsVec.end());
	}

	// ________________________________________________________________________ //
	// __Dopasowanie do Gaussiana______________________________________________ //
	// ________________________________________________________________________ //

	// Kopiowanie pełnych długości fal i rozmiarów
	cl::copy(queue, wavelengthsMatrixHost.begin(), wavelengthsMatrixHost.end(), wavelengthsMatrix);
	cl::copy(queue, sizesHost.begin(), sizesHost.end(), sizes);

	// Obliczenie chi kwadrat
	std::vector<cl_float> gaussianChisqsVec(size);
	{
		auto gaussianChisqs = utils::make_buffer(context, CL_MEM_READ_WRITE, gaussianChisqsVec);
		calcGaussianChisq(wavelengthsMatrix, spectrumsEmissionLinesCopy, errorsMatrix, fitGResults, sizes, size,
				gaussianChisqs);
		cl::copy(queue, gaussianChisqs, gaussianChisqsVec.begin(), gaussianChisqsVec.end());
	}

	// ________________________________________________________________________ //
	// __Obliczenie FWHM_______________________________________________________ //
	// ________________________________________________________________________ //

	std::vector<cl_float> gaussianFWHMsVec(size);
	{
		// Obliczamy
		auto gaussianFWHMs = utils::make_buffer(context, CL_MEM_READ_WRITE, gaussianFWHMsVec);
		calcGaussianFWHM(fitGResults, gaussianFWHMs, size);
		cl::copy(queue, gaussianFWHMs, gaussianFWHMsVec.begin(), gaussianFWHMsVec.end());
	}

	return
	{
		fitGResultsVec,
		ewsVec,
		gaussianChisqsVec,
		gaussianFWHMsVec
	};
}

/**
 *
 * @param ys
 * @param xs
 * @param sizes
 * @param width
 * @param results
 * @param events
 * @param event
 */
void SpecParameterization::fitGaussian(cl::Buffer& ys, cl::Buffer& xs,
		cl::Buffer& sizes, const size_t size, cl::Buffer& results,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->fitGaussianKernel.setArg(arg++, sizeof(cl_float *), &ys);
	this->fitGaussianKernel.setArg(arg++, sizeof(cl_float *), &xs);
	this->fitGaussianKernel.setArg(arg++, static_cast<cl_uint>(size));
	this->fitGaussianKernel.setArg(arg++, sizeof(cl_uint *), &sizes);
	this->fitGaussianKernel.setArg(arg++, static_cast<cl_uint>(MAX_FITGAUSSIAN_LM_ITERS)); // maksymalna liczba iteracji
	this->fitGaussianKernel.setArg(arg++, sizeof(cl_float4 *), &results);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = size;
	size_t remainder = global_size_d1 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if (global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->fitGaussianKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 *
 * @param xs
 * @param gaussianParams
 * @param sizes
 * @param width
 * @param max_spectrum_size
 * @param fxs
 * @param events
 * @param event
 */
void SpecParameterization::calcGaussian(cl::Buffer& xs, cl::Buffer& gaussianParams,
		cl::Buffer& sizes, const size_t size, const size_t max_spectrum_size, cl::Buffer& fxs,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->calcGaussianKernel.setArg(arg++, sizeof(cl_float *), &xs);
	this->calcGaussianKernel.setArg(arg++, sizeof(cl_float4 *), &gaussianParams);
	this->calcGaussianKernel.setArg(arg++, static_cast<cl_uint>(size));
	this->calcGaussianKernel.setArg(arg++, sizeof(cl_uint *), &sizes);
	this->calcGaussianKernel.setArg(arg++, sizeof(cl_float *), &fxs);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d2 = max_spectrum_size;
	size_t remainder = global_size_d2 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d2 += device_max_wg_size - remainder;
	}
	if (global_size_d2 < max_spectrum_size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(size, global_size_d2);
	cl::NDRange local(1, device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->calcGaussianKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

void SpecParameterization::calcGaussianChisq(cl::Buffer& xs, cl::Buffer& ys, cl::Buffer& errors,
		cl::Buffer& gaussianParams,
		cl::Buffer& sizes, const size_t size, cl::Buffer& results,
		const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->calcGaussianChisqKernel.setArg(arg++, sizeof(cl_float *), &xs);
	this->calcGaussianChisqKernel.setArg(arg++, sizeof(cl_float *), &ys);
	this->calcGaussianChisqKernel.setArg(arg++, sizeof(cl_float *), &errors);
	this->calcGaussianChisqKernel.setArg(arg++, sizeof(cl_float4 *), &gaussianParams);
	this->calcGaussianChisqKernel.setArg(arg++, static_cast<cl_uint>(size));
	this->calcGaussianChisqKernel.setArg(arg++, sizeof(cl_uint *), &sizes);
	this->calcGaussianChisqKernel.setArg(arg++, sizeof(cl_float *), &results);

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = size;
	size_t remainder = global_size_d1 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if (global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->calcGaussianChisqKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

/**
 *
 * @param gaussianParams
 * @param gaussianFWHMs
 * @param size
 * @param events
 * @param event
 */
void SpecParameterization::calcGaussianFWHM(cl::Buffer& gaussianParams, cl::Buffer& gaussianFWHMs,
		const size_t size, const std::vector<cl::Event>* events, cl::Event* event)
{
	unsigned int arg = 0;
	this->calcGaussianFWHMKernel.setArg(arg++, sizeof(cl_float *), &gaussianParams);
	this->calcGaussianFWHMKernel.setArg(arg++, sizeof(cl_float *), &gaussianFWHMs);
	this->calcGaussianFWHMKernel.setArg(arg++, static_cast<cl_uint>(size));

	cl::Device device = this->queue.getInfo<CL_QUEUE_DEVICE>();
	size_t device_max_wg_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
	// Ustawiam dla work-group na drugim wymiarze maks. rozmiar, żeby
	// zapewnić łączony odczyt danych.
	//
	// Zwykle device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() jest równe 256,
	// a sterownik wtedy często ustawia local_workgroup_size na 16x16, co jest w tym
	// przypadku tragiczne.

	size_t global_size_d1 = size;
	size_t remainder = global_size_d1 % device_max_wg_size;
	if (remainder != 0)
	{
		global_size_d1 += device_max_wg_size - remainder;
	}
	if (global_size_d1 < size)
	{
		throw std::runtime_error("Error in calculating global_work_size.");
	}

	cl::NDRange global(global_size_d1);
	cl::NDRange local(device_max_wg_size);

	this->queue.enqueueNDRangeKernel(
			this->calcGaussianFWHMKernel,
			cl::NullRange,
			global,
			local,
			events,
			event);
}

} /* namespace ocl */

} /* namespace algorithm */

