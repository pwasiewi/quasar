#pragma once

#define MAX_FITGAUSSIAN_LM_ITERS 500

#include "../defines.hpp"

#include "../utils.hpp"
#include "../spectools/SpecTools.hpp"
#include "../tools/Tools.hpp"
#include "../basicmatrix/BasicMatrix.hpp"
#include "../mavg/MAVG.hpp"

#include "../fefitwin/FeFitWin.hpp"
#include "../continuum/Continuum.hpp"

#include "../SpectralLine.hpp"

#include "quasar/Quasar/Quasar.hpp"

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include <CL/cl.hpp>

#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <algorithm>

namespace algorithm
{

namespace ocl
{

class SpecParameterization
{
	public:

		struct FitElementResults
		{
				std::vector<cl_float4> fitParams;
				std::vector<cl_float> ews;
				std::vector<cl_float> chisqs;
				std::vector<cl_float> gaussian_fwhms;

				bool success(size_t elementIdx)
				{
					if(fitParams.size() < (elementIdx+1))
						return false;
					else
						return fitParams[elementIdx].s[3] > 0.0;
				}
		};

		/**
		 * Struktura dla zwracania wyników.
		 */
		struct Results
		{
				// Wyniki dotyczące continuum

				// Dopasowanie
				std::vector<cl_float> chisqs;
				// Parametry prostej regresji
				std::vector<cl_float8> continuum_reglin_results;
				// Parametry prostej regresji
				std::vector<cl_float8> reglin_results;

				// Wyniki dotyczące szablonu żelaza
				std::vector<cl_float> scaleRates;
				std::vector<cl_uint>  sizes_fewindows;
				std::vector<cl_float> reducedChisqs_fewindows;
				std::vector<cl_float> reducedChisqs_full;
				std::vector<cl_float> ews_full;
				std::vector<cl_float> reducedChisqs_feRange;
				std::vector<cl_float> ews_feRange;

				// Wyniki dotyczące dopasowania elementów
				std::vector<FitElementResults> elementsFits;
		};

		SpecParameterization(const cl::Context&, const cl::CommandQueue&);
		virtual ~SpecParameterization();

		SpecParameterization::Results run(std::vector<Quasar> astronomicalObjs,
				std::vector<SpectralLine> elements,
				std::vector<cl_float2> continuumWindows, cl_float ampWavelength,
				std::vector<cl_float2> feWindows, FeTemplate feTemplate,
				FeFitWin::FitParameters fitParameteres = { 1600.0f, 900.0f, 1.0f,
						{ { 2200.0f, 2650.0f } }, false, FeFitWin::Type::WIN },
				const std::vector<cl::Event>* events = nullptr);

	private:

		SpecParameterization::FitElementResults fitElement(
				cl::Buffer& spectrumsEmissionLines, cl::Buffer& spectrumsEmissionLinesCopy,
				cl::Buffer& continuumsMatrix, /**cl::Buffer& dcontinuumsMatrix, */
				cl::Buffer& wavelengthsMatrix, std::vector<cl_float> wavelengthsMatrixHost,
				cl::Buffer& errorsMatrix,
				cl::Buffer& sizes, std::vector<cl_uint> sizesHost,
				size_t size, SpectralLine element);

		void fitGaussian(cl::Buffer& ys, cl::Buffer& xs,
				cl::Buffer& sizes, const size_t width, cl::Buffer& results,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void calcGaussian(cl::Buffer& xs, cl::Buffer& gaussianParams,
				cl::Buffer& sizes, const size_t width, const size_t max_spectrum_size, cl::Buffer& fxs,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void calcGaussianChisq(cl::Buffer& xs, cl::Buffer& ys, cl::Buffer& errors,
				cl::Buffer& gaussianParams,
				cl::Buffer& sizes, const size_t width, cl::Buffer& results,
				const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);
		void calcGaussianFWHM(cl::Buffer& gaussianParams, cl::Buffer& gaussianFWHMs,
				const size_t size, const std::vector<cl::Event>* events = nullptr, cl::Event* event = nullptr);

		/** OpenCL */

		const cl::Context context;
		const cl::CommandQueue queue;

		cl::Program program;
		cl::Program::Sources sources;
		static std::vector<std::string> ssources;

		/** Narzędzia */

		Tools tools;
		SpecTools specTools;
		BasicMatrix basicMatrix;
		MAVG mavg;
		Continuum ctm;
		FeFitWin fefit;

		/** Kernele, czyli różne operacje */

		cl::Kernel fitGaussianKernel;
		static const char* fitGaussianKernelName;

		cl::Kernel calcGaussianKernel;
		static const char* calcGaussianKernelName;

		cl::Kernel calcGaussianChisqKernel;
		static const char* calcGaussianChisqKernelName;

		cl::Kernel calcGaussianFWHMKernel;
		static const char* calcGaussianFWHMKernelName;

		/** End OpenCL */
};

} /* namespace ocl */

} /* namespace algorithm */
