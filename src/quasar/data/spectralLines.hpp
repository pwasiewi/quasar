#pragma once

#include "quasar/algorithm/SpectralLine.hpp"
#include <vector>

namespace data
{

/**
 *
 */
const std::vector<SpectralLine> spectralLines =
		{
				{ std::string("Ha"), { 6480.0f, 6660.0f }, { 5.0e-17, 6562.8f, 20.0f, 0.0f} },
				{ std::string("Hb"), { 4800.0f, 4900.0f }, { 1.0e-17, 4861.0f, 20.0f, 0.0f} },
				{ std::string("MgII"), { 2750.0f, 2850.0f }, { 1.0e-17, 2800.0f, 20.0f, 0.0f} },
				{ std::string("CIII"), { 1880.0f, 1930.0f }, { 5.0e-17, 1909.0f, 10.0f, 0.0f} },
				{ std::string("CIV"), { 1520.0f, 1570.0f }, { 5.0e-17, 1549.0f, 10.0f, 0.0f} },
				{ std::string("SiIV"), { 1370.0f, 1430.0f }, { 1.0e-17, 1400.0f, 10.0f, 0.0f} },
				{ std::string("Lya"), { 1190.0f, 1250.0f }, { 5.0e-17, 1216.0f, 20.0f, 0.0f} }
		};

} /* namespace data */
