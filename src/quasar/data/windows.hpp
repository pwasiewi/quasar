#pragma once

#ifndef __CL_ENABLE_EXCEPTIONS
#define __CL_ENABLE_EXCEPTIONS
#endif
#include "CL/cl.hpp"

#include <vector>

namespace data
{

/**
 * Okna widmowe do fitowania kontinuum.
 * Nie spodziewamy się w nich linii emisyjnych [A]
 */
const std::vector<cl_float2> contwinfull =
		{
				{ 1140., 1150. },
				{ 1275., 1280. },
				{ 1320., 1330. },
				{ 1455., 1470. },
				{ 1690., 1700. },
				{ 2160., 2180. },
				{ 2225., 2250. },
				{ 3010., 3040. },
				{ 3240., 3270. },
				{ 3790., 3810. },
				{ 4210., 4230. },
				{ 5080., 5100. },
				{ 5600., 5630. },
				{ 5970., 6000. }
		};

/**
 * Okna zdominowane przez emisje żelaza [A]
 */
const std::vector<cl_float2> fewinfull =
		{
				{ 2020., 2120. },
				{ 2250., 2650. },
				{ 2900., 3000. },
				{ 4400., 4750. },
				{ 5150., 5500. }
		};

} /* namespace data */
