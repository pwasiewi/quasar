#pragma once

#define TOKEN_PASTEx(x, y) x ## y
#define TOKEN_PASTE(x, y) TOKEN_PASTEx(x, y)

/**
 * Strażnik - obiekt wykonujący podaną funkcje podczas swojej destrukcji.
 */
template<typename T>
class AutoOutOfScope
{
	public:

		AutoOutOfScope(T& destructor) :
					m_destructor(destructor),
					m_dismiss(false)
		{
		}

		~AutoOutOfScope()
		{
			if (!m_dismiss)
			{
				m_destructor();
			}
		}

		/**
		 * Wycofanie strażnika - podczas destrukcji
		 * żadna akcja nie zostanie wykonana.
		 */
		void dismiss()
		{
			this->m_dismiss = true;
		}

	private:
		/**	Funkcja do wykonania przy destrukcji.	*/
		T& m_destructor;
		/**	Flaga wycofania.	*/
		bool m_dismiss;
};

// Deklaracje dla strażnika nazwanego.
// Strażnika nazwanego można wycofać.

#define Guard_INTERNAL(Name, Destructor, counter) \
    auto TOKEN_PASTE(auto_func_, counter) = [&]() { Destructor }; \
    AutoOutOfScope<decltype(TOKEN_PASTE(auto_func_, counter))> Name(TOKEN_PASTE(auto_func_, counter));

/**
 * Strażnik nazwany - obiekt wykonujący podaną funkcje podczas swojej destrukcji.
 * Można go wycofać, tj. anulować wykonanie podanej funkcji przy destrukcji.
 */
#define Guard(Name, Destructor) Guard_INTERNAL(Name, Destructor, __COUNTER__)

// Deklaracje dla strażnika nienazwanego.
// Strażnika nienazwanego nie można wycofać.

#define Auto_INTERNAL(Destructor, counter) \
    auto TOKEN_PASTE(auto_func_, counter) = [&]() { Destructor }; \
    AutoOutOfScope<decltype(TOKEN_PASTE(auto_func_, counter))> TOKEN_PASTE(auto_, counter)(TOKEN_PASTE(auto_func_, counter));

/**
 * Strażnik nienazwany - obiekt wykonujący podaną funkcje podczas swojej destrukcji.
 * Nie można go wycofać.
 */
#define AutoGuard(Destructor) Auto_INTERNAL(Destructor, __COUNTER__)
