# Comments on code #

## "Quasar" instead of "Astronomical object" ##

* Astronomical object is represented by class named "Quasar".
* In variables, macros, classes names and in comments word "quasar" is frequently used instead of "astronomical object", and character "q" is frequently used as variables, classes names prefix.

It's because of code legacy - originally this program was made to analyze quasars.


# Uwagi, co do kodu #

## "Quasar" zamiast "Astronomical object" ##

* Obiekt astronomicznych reprezentowany jest przez klasę o nazwie "Quasar".
* W nazwach zmiennych, klas, makr itp. i w komentarzach itp. słowo "kwazar" jest często używane tam, gdzie można by spodziewać "obiekt astronomiczny", a litera "q" (od "quasar") jest często używana jako przedrostek nazw zmiennych, klas.

Jest to spowodowane tym, że oryginalnie program miał analizować wyłącznie kwazary, a nie bardziej ogólnie - obiekty astronomiczne (np. aktywne galatyki).




